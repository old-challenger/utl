// ---------------------------------------------------------------------------------------------------------
#ifndef UTL_SHARED_TEMPLATE_H_MRV
#define UTL_SHARED_TEMPLATE_H_MRV
// ---------------------------------------------------------------------------------------------------------
/*!   \file		   uShared.h
      \author	   Ruslan V. Malstev ( mrv.work.box@yandex.ru )
      \date		   27.10.2009
      \version	   0.2
      \brief	   Шаблон класса shared< class data_t, bool multiThread_p > для хранения данных с поддержкой 
                  копирования ссылок при вызове operator = () и автоматичского удалеия объекта при удалении 
                  всех ссылок на него. Реализована поддержка многопоточности.
      \copyright  GNU Public License
*/
// ---------------------------------------------------------------------------------------------------------
#include <uMutex.h>
//#include <QMutex>
// ---------------------------------------------------------------------------------------------------------
#include <utility>
#include <assert.h>
// ---------------------------------------------------------------------------------------------------------
namespace utl {
// ---------------------------------------------------------------------------------------------------------
//typedef QMutex mutex;
//template< class mutex_class = mutex >
//class locker
//{
//private:
//   mutex_class & mutex_m;
//   // ------------------------------------------------------------------------------------------------------
//public:
//   locker ( mutex_class & mutex_p ) : mutex_m( mutex_p ) { mutex_m.lock(); }
//   // ------------------------------------------------------------------------------------------------------
//
//   ~locker () { mutex_m.unlock(); }
//   // ------------------------------------------------------------------------------------------------------
//}; // class locker
//// ---------------------------------------------------------------------------------------------------------

/*!	\brief  Основной шаблон-класс хнанения данных.

      Предназначен для хранения множества копий произвольных данных. При созданиии новой копии (вызове (!) 
   конструктора копирования или оператора присваивания) происходит копирование только указателя на данные, 
   а не всей структуры данных \b data_t. Чтобы создать новый экземпляр данных необходимо вызвать 
   функцию void makeNew().
*/
template < class data_t, bool multiThread_p = false >
class shared 
{
protected:
   /*!	\brief Класс для хранения множества копий без выделения новой памяти.

         Данный класс предназначен только для использования внутри шаблон-класса 
      \b utl::shared< class data_t > и его наследников. 
   */
   class hidden
   {
      friend class shared;
   protected:
      std::size_t	counter_m;  ///< Число копий данных
      data_t *	   baseData_m; ///< Указатель на структуру хранимых данных
      // ---------------------------------------------------------------------------------------------------

      /*!   \brief Коструктор формирующий оболочку для уже созданных данных

            Создается оболочка для уже созданных данных <b>data_p</b>. Количество копий 
         <e>hiddenShared< data_t >::copyCounter_m == 1</e>.
      */
      hidden ( data_t * data_p ) : counter_m( 1 ), baseData_m( data_p ) {}
      // ---------------------------------------------------------------------------------------------------

      /*!	\brief Деструктор
      */
      ~hidden () { delete baseData_m; }
      // ---------------------------------------------------------------------------------------------------
   }; // class hidden
   // ------------------------------------------------------------------------------------------------------
   

   /*!	\brief Класс для хнанения множества копий без выделения новой памяти (многопоточная версия).

         Данный класс предназначен только для использования внутри шаблон-класса 
      \b utl::shared< class data_t > и его наследников. 
   */
   class hiddenMulti : public hidden 
   {
      friend class shared;
   public:
      mutex * mutex_m; ///< Указатель на мьютекс для потоковой синхронизации доступа к атрибутам hidden
      // ---------------------------------------------------------------------------------------------------
      hiddenMulti ( data_t * data_p, mutex * mutex_p ) : hidden( data_p ), mutex_m( mutex_p ) 
      {  assert( NULL != mutex_m ); }
      // ---------------------------------------------------------------------------------------------------
   }; // class hiddenMulti
   // ------------------------------------------------------------------------------------------------------
   mutable hidden * data_m;
   // ------------------------------------------------------------------------------------------------------

   /*!	\brief Создание структуры данных shared pointer'а для уже имеющегося экземляра данных
         \param data_p - указатель на экземпляр данных

         Если указатель data_p равен NULL, выделяется память под объект класса data_t( вызывается 
      конструктор без параметров), в противном случае используется указатель на уже существующий объект. 
      Полученный указатель помещается в новую структуру для хнанения множества копий (выделяется память), 
      счётчик копий усатавливается равным 1. Если включена потокова синхронизация для хранения множества 
      копий используется структура hiddenMulti, в противном случае - hidden.
   */
   void createHidden ( data_t * data_p = NULL ) const 
   { 
      data_t * const data_l = NULL == data_p ? new data_t() : data_p;
      data_m = multiThread_p ? new hiddenMulti( data_l, new mutex() ) : new hidden( data_l );
   }
   // ------------------------------------------------------------------------------------------------------

   /*!	\brief Создание структуры данных shared pointer'а с копией уже имеющегося экземляра данных
         \param data_p - константная ссылка экземпляр данных

         Выделяется память для копии объекта data_p и для полученного указателя вызывается функция 
      createHidden().
   */
   void createCopy ( const data_t & data_p ) { createHidden ( new data_t( data_p ) ); }
   // ------------------------------------------------------------------------------------------------------

   /*!   \brief Подключение данной ссылки к существующему экземляру данных
         \param data_p - указатель на структуру hidden, обеспечивающую хранение множества ссылок

         Подключает текущую ссылку к экзепляру данных hidden::baseData_m и увеличивает счётчик ссылок 
      на единицу. Если включена потоковая синхронизауия данная операция блокируется с помощью мьютекса 
      (( hiddenMulti * ) data_p )->mutex_m.
   */
   void attach ( hidden * data_p ) 
   { 
      if ( NULL == data_p ) return ; 
      if ( multiThread_p ) static_cast< hiddenMulti * >( data_p )->mutex_m->lock();
      assert( NULL == data_m && NULL != data_p->baseData_m );
      ++data_p->counter_m;
      data_m = data_p;
      if ( multiThread_p ) static_cast< hiddenMulti * >( data_p )->mutex_m->unlock();
   }
   // ------------------------------------------------------------------------------------------------------

   /*!   \brief Отключение текущей ссылки от экземляра данных

         Отключает текущую ссылку от экземпляра данных и уменьшает счётчик ссылок на единицу. Указатель на 
      структуру данных обеспечивающую хранение множества ссылок получает значение NULL. Если счётчик ссылок 
      становится равным нулю, экземляр данных уничтожается (оператор delete). Если включена потоковая 
      синхронизация, операция блокируется с помощью мьютекса (( hiddenMulti * ) data_p )->mutex_m.
   */
   void detach () 
   { 
      if ( NULL == data_m ) return ;
      mutex * const mutex_l = multiThread_p ? static_cast< hiddenMulti * >( data_m )->mutex_m : NULL;
      if ( NULL != mutex_l ) mutex_l->lock();
      const bool remove_cl = !--data_m->counter_m;
      if ( remove_cl ) delete data_m;
      data_m = NULL;
      if ( NULL != mutex_l ) 
      {
         mutex_l->unlock();
         if ( remove_cl ) delete mutex_l;
      }
   }
   // ------------------------------------------------------------------------------------------------------
public:

   /*!	\brief Конструктор без параметров 
         \param create_p - создать экземпляр данных

         Только создание объекта shared. По умолчанию память экзепляр данных data_t не выделяется, для 
      создания объекта data_t необходимо передать параметр create_p = true. 
   */
   shared ( bool create_p = false ) : data_m( NULL ) { if ( create_p ) createHidden(); } 
   // ------------------------------------------------------------------------------------------------------

   /*!	\brief Коструктор копирования 
         \param etalon_p - константная ссылка на существующию объект-ссылку shared.

         Копирование указателя на экзепляр данных и увеличение счетчика копий. Если данные у копируемого 
      объекта shared отсутствуют (см. isNull()) новый экземпляр данных не создается.
   */
   shared ( const shared< data_t, multiThread_p > & etalon_p ) : data_m( NULL ) 
   {  attach( etalon_p.data_m ); }
   // ------------------------------------------------------------------------------------------------------

   /*!	\brief Коструктор, формирующий ссылку на уже созданный экземляр данных
         \param data_p - указатель на экземпляр данных
         
         Создание нового множества (с мощьностью 1) ссылок на существующий экземляр данных data_p. Если 
      указатель data_p равен нулю, ссылка остаётся пустой (множество ссылок не создаётся).
   */
   shared ( data_t * data_p ) : data_m( NULL ) { if( NULL != data_p ) createHidden( data_p ); }
   // ------------------------------------------------------------------------------------------------------

   /*!	\brief   Коструктор, предоставляющий возможность создания нового экземпляра данных, идентичного 
                  заданному
         \param   etalon_p - копируемые данные

         Создаётся ссылка на копию объекта etalon_p, т.е. выделяется память под новый объект data_t и 
      содаётся новое множество ссылок (с мощьностью 1) на полученный указатель data_t *.
   */
   shared ( const data_t & etalon_p ) { createCopy( etalon_p ); }
   // ------------------------------------------------------------------------------------------------------

   /*!	\brief Деструктор

         Удаление копии хранимых данных. Осоединение от множества ссылок на экземпляр данных, уменьшение 
      счётчика ссылок и удаление самого экземпляра данных в случае если счётчик ссылок стал равным нулю.
   */
   virtual ~shared () { detach(); }
   // ------------------------------------------------------------------------------------------------------

   /*!	\brief Функция создания нового экземпляра данных 

         Отсоединение от текущего экзепляра данных и создание нового экземпляра данных и множетва ссылок на 
      него.
   */
   void makeNew ( data_t * data_p = NULL ) {
      detach();
      createHidden( data_p );
   }
   // ------------------------------------------------------------------------------------------------------

   /*!   \brief Cделать полную копию данных
         \return Ссылка shared на новую копию экземпляра данных

         В отличие от оператора присваивания, копирующего только ссылку на объект data_t, данная функция 
      выделяет память и вызывает конструктор копирования класса data_t.
   */
   shared< data_t, multiThread_p > fullCopy () const {  
      return NULL == data_m   ? shared< data_t, multiThread_p >() 
                              : shared< data_t, multiThread_p >( *data_m->baseData_m ); 
   }
   // ------------------------------------------------------------------------------------------------------

   /*!	\brief Проверка на пустую ссылку
         \return true - если экземпляр данных отсутствует, иначе - false
   */
   bool isNull () const { return NULL == data_m; }
   // ------------------------------------------------------------------------------------------------------

   /*!	\brief Создать объект shared не содержащий данных 

         Функциональность аналогична вызову конструктора shared( bool create_p ) со значением 
      create_p == false.
   */
   void setNull () { detach(); }
   // ------------------------------------------------------------------------------------------------------

   /*!	\brief Константная функция доступа к данным
         \return Константная ссылка на хранимый экзепляр данных. 
   */
   const data_t & constData () const {
      if ( NULL == data_m ) createHidden();
      return *( data_m->baseData_m ); 
   }
   // ------------------------------------------------------------------------------------------------------

   /*!	\brief Функция доступа к данным
         \return Неконстантная ссылка на хранимый экзепляр данных. 
   */
   data_t & data () { return const_cast< data_t & >( constData() ); }
   // ------------------------------------------------------------------------------------------------------

   /*!	\brief Количество созданных копий хранимого экзепляра данных
         \return Значение переменной \b utl::shared< type_t >::data_m->copyCounter_m.
   */
   std::size_t copyCounter () const { return NULL == data_m ? 0 : data_m->counter_m; }
   // ------------------------------------------------------------------------------------------------------

   /*!	\brief	Оператор присваивания
         \param	Etalon_p константная ссылка на объект с которого будет осуществляться копирование
         \return	Неконстантную ссылку на настоящий объект после присваивания нового значения

         Отсоединение от старого экземпляра данных, копирование указателя на присваеваемые даные, увеличение 
      счетчика копий в множестве ссылок экзепляра данных etalon_p. 
   */
   virtual shared< data_t, multiThread_p > & operator = ( const shared< data_t, multiThread_p > & etalon_p ) {
      detach();
      attach( etalon_p.data_m ); 
      return *this;
   }
   // ------------------------------------------------------------------------------------------------------

   /*!	\brief	Оператор проверки тождества (равенства) двух величин
         \param	etalon_p константная ссылка на объект shared, с которым будет осуществляться сравнение
         \return	- \b true - если указатели на экзепляры данных совпадают;
                  - \b false - во всех остальных случаях.

         Сравниваются указатели на хранимые данные, если объекты ссылкаются на один и тот же экзепляр 
      данных, они считаются равными.
   */
   virtual bool operator == ( const shared< data_t, multiThread_p > & etalon_p ) const 
   {  return data_m == etalon_p.data_m; }
   // ------------------------------------------------------------------------------------------------------

   /*!	\brief	Оператор проверки неравенства двух величин
         \param	etalon_p - константная ссылка на объект shared, с которым будет осуществляться сравнение
         \return	- \b true - если указатели на экзепляры данных не совпадают;
                  - \b false - если указатели на данные совпадают.

         Сравниваются указатели на хранимые данные, если объекты ссылкаются на разные экзепляры данных, 
      они считаются не равными.
   */
   virtual bool operator != ( const shared< data_t, multiThread_p > & etalon_p ) const
   {	return data_m != etalon_p.data_m; }
   // ------------------------------------------------------------------------------------------------------

   /*!	\brief	Оператор сравнения меньше
         \param	etalon_p - константная ссылка на объект shared, с которым будет осуществляться сравнение
         \return	- \b true - если указатель на данные настоящего объекта меньше указателя на данные 
                              объекта etalon_p, 
                  - \b false - во всех остальных случаях

         Данный оператор введен с целью обеспечения возможности использования объектов данного шаблон-класса 
      в контейнерах типа std::map<> (см. документацию по билиотеке STL).
   */
   virtual bool operator < ( const shared< data_t, multiThread_p > & etalon_p ) const 
   {  return data_m < etalon_p.data_m; }
   // ------------------------------------------------------------------------------------------------------
}; // class shared 
// ---------------------------------------------------------------------------------------------------------
}  // namespace utl {
// ---------------------------------------------------------------------------------------------------------
#endif // UTL_SHARED_TEMPLATE_H_MRV
// ---------------------------------------------------------------------------------------------------------
