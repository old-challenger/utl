// ------------------------------------------------------------------
#ifndef USEFUL_SOCKET_CLASS_H_MRV
#define USEFUL_SOCKET_CLASS_H_MRV
// ------------------------------------------------------------------
/*!   \file		   uSocket.h
      \author     MRV (mrv.work.box@yandex.ru)
      \date		   13.02.2008
      \version	   2.05
      \brief		Классы для установления соединений и передачи данных по протоколам TCP/UDP.
      \copyright  GNU Public License

	Список дополнений и изменений
	v2.02:
	1. [created]	В класс utl::cTCPbase добавлена статическая функция получения описания ошибке по коду
	2. [created]	Не статическая функция utl::cTCPbase::errorName() возвращает описание последней ошибки

	v2.03:
	1. [fixed]		Баг с вылетом при проверке на установленное соединение в функции  int cTCPclient::nextDataWrite()
	2. [created]	Добавлена функция cTCPbase::prepareSetedSocket()
	3. [created]	Добавлена функция cTCPclient::disconnect()

	v2.04:
	1. [created]	Добавлины функции очистки входных и выходных очередей
	
	v2.05
	1. [created]	В класс	utl::cTCPclient добавлена функция utl::cTCPclient::nextDataChange(),
					объединяющая функциональность utl::cTCPclient::nextDataRead() и
					utl::cTCPclient::nextDataWrite().
	2. [created]	В класс	utl::cTCPserver добавлена функция utl::cTCPserver::nextDataChange(),
					объединяющая функциональность utl::cTCPserver::nextDataRead() и
					utl::cTCPserver::nextDataWrite().
	3. [created]	Функции utl::cTCPclient::nextDataChange(), utl::cTCPclient::nextDataRead(),
					utl::cTCPclient::nextDataWrite() переписаны с использованием добавленных в класс 
					utl::cTCPclient функций utl::cTCPclient::readSocket() и utl::cTCPclient::writeSocket().
	4. [created]	Функции utl::cTCPserver::nextDataChange(), utl::cTCPserver::nextDataRead(),
					utl::cTCPserver::nextDataWrite() переписаны с использованием добавленных в класс 
					utl::cTCPserver функций utl::cTCPclient::readScks() и utl::cTCPclient::writeScks().
*/
// ------------------------------------------------------------------
#include <winsock.h> 
// ------------------------------------------------------------------
#include <string>
#include <queue>
// ------------------------------------------------------------------
namespace utl
{
typedef	std::queue< std::string > messagesQueue_t;	///< Очередь сообщений

/*!	\brief Данных о соединении сервера с клиентом.

	Используется классом utl::cTCPserver для хранения информации 
	по установленным соединениям
*/
class	cClientData
{
	friend class cTCPserver; //
public:

	/*! \brief Направления передачи данных */
	enum eDirections
	{
		NO_TRANSMISSIONS	= 0,	///< Данные этому клиенту не передаются и не принимаются
		READ_DATA			= 1,	///< Данные только принимаюся от клиента
		WRITE_DATA			= 2,	///< Данные только передаются клиенту
		READ_WRITE_DATA		= 3,	///< Данные и передаются и принимаются
	};

protected:
	SOCKET			pSocket;		///< Переменная-сокет для соединения с клиентом
	sockaddr_in		strctAddr;		///< Адрес сокета
	eDirections		iDataDirection;	///< Влаг направления передачи данных

public:
	/*! \brief Конструктор
	
		Данные обнуляются, сокет не создается
	*/
	cClientData() :
		pSocket( INVALID_SOCKET ),
		iDataDirection( READ_WRITE_DATA )	
	{
		strctAddr.sin_family			= AF_INET;
		strctAddr.sin_addr.S_un.S_addr	= 0;
		strctAddr.sin_port				= 0;
	}

	/*!	\brief Конструктор копирования 
		
		Данные копируются, новый сокет не создается
	*/
	cClientData( const cClientData&	Etalon_p ) :
		pSocket( Etalon_p.pSocket ),
		iDataDirection( Etalon_p.iDataDirection )	
	{
		strctAddr.sin_family			= Etalon_p.strctAddr.sin_family;
		strctAddr.sin_addr.S_un.S_addr	= Etalon_p.strctAddr.sin_addr.S_un.S_addr;
		strctAddr.sin_port				= Etalon_p.strctAddr.sin_port;
	}

	/*!	\brief Изменить направление передачи данных 
	*/
	void setDataDirecition ( eDirections enmDircetion_p )	
	{	iDataDirection = enmDircetion_p; }

	/*! \brief Узнать направление передачи данных 
	*/
	eDirections	dataDirection() const
	{	return iDataDirection; }
};

/*!	\brief Базовый класс для создания соединения.

	Содержит основные настройки для соединения по протоколу TCP.
*/
class cTCPbase
{
public:
	/// Список ошибок возникающих в процессе работы объекта
	enum eErrorTypes
	{
		NO_ERRORS				= 0,	///< Ошибок нет
		INVALID_SOCKET_ERROR	= 1,	///< Плохой сокет
		CAN_NOT_BIND_ERROR		= 2,	///< Ошибка привязки адреса к сокету
		CONNECT_FUNCTION_ERROR	= 3,	///< Ошибка при попытке соединения
		SELECT_FUNCTION_ERROR	= 4,	///< Ошибка при вызве функции select
		TIME_OUT_HEPPENED		= 5,	///< Timeout
		RECIEVE_FUNCTION_ERROR	= 6,	///< Ошибка при получении данных
		SEND_FUNCTION_ERROR		= 7,	///< Ошибка при отправке данных
		OUT_QUEUE_EMPTY			= 8,	///< Очередь на отправку пуста
		ALREADY_CONNECTED		= 9,	///< Уже есть соединение
		ALREADY_LISTERNING		= 10,	///< Уже слушает
		NOT_CONNECTED			= 11,	///< Нет соединения
		SOCKET_CTRL_ERROR		= 12,	///< Ошибка при попытке перевода сокета в блокирующий режим
		ACCEPT_FUNCTION_ERROR	= 13,	///< Ошибка при попытке вызова accept
		SHUT_DOWN_ERROR			= 14	///< Ощибка закрытия сокета
	};
protected:
	int				iErrCode;			///< Код последней возникшей ошибки.
					
	SOCKET			pWorkSocket;		///< Сокет.
	sockaddr_in		strctLocalAddr;		/*!	Структура, содержащая информацию о локальном адресе:
										IP-адрес и номер порта см. функцию utl::cTCPbase::setLocalAddr.
										(IP имеет смысл устанавливать если в наличии несколько 
										сетевых интерфейсов, и по ним планируется получать разные 
										данные, иначе можно оставить = 0.0.0.0; номер порта следует 
										указывать обязательно).	*/
	unsigned int	uiBufferSize;		///< Максимальный размер принимаемых за раз сообщений
	timeval			tTimeOut;			///< Таймаут 

	messagesQueue_t	que_strIn;		///< Очередь полученных сообщений
	messagesQueue_t	que_strOut;		///< Очередь сообщений на передачу

	/*!	\brief	Подготовить сокет к работе
		\param	ui16PortNumber_p - номер порта настроаиваемого сокета,
		\param	uiIP_p - IP адрес интефейса на который ориентируется настаиваемый сокет,
		\return Код ошибки см. utl::cTCPbase::eErrorTypes

		Создается сокет за ним закрепляется заданный адрес. Если попытка установки адреса 
		не удалась сокет закрывается.
	*/												  
	int prepareSocket( unsigned short int ui16PortNumber_p = 0, unsigned int uiIP_p = 0 )   
	{
		// Заполнение структуры адреса
		strctLocalAddr.sin_family			= AF_INET;
		strctLocalAddr.sin_port				= htons( ui16PortNumber_p );
		strctLocalAddr.sin_addr.S_un.S_addr	= uiIP_p;
		// 
		return prepareSetedSocket();
	}

	/*!	\brief	Подготовить к работе сокет с заранее установленным адресом
		\return Код ошибки см. utl::cTCPbase::eErrorTypes

		Действие аналогично предыдущей функции за исключением переопределения адресных данных.
	*/												  
	int prepareSetedSocket()
	{
		// Создание сокета
		pWorkSocket = socket (AF_INET, SOCK_STREAM, 0);
		// Проверка на корректность выполнения процедуры создания
		if ( pWorkSocket == INVALID_SOCKET || pWorkSocket == SOCKET_ERROR )
			return ( iErrCode = eErrorTypes::INVALID_SOCKET_ERROR );
		// Связь сокета с IP адресом и портом
		if ( bind ( pWorkSocket, (sockaddr *) &strctLocalAddr, sizeof( sockaddr ) ) )
		{
			closesocket( pWorkSocket );
			iErrCode = eErrorTypes::CAN_NOT_BIND_ERROR;
		}
		else
			iErrCode = eErrorTypes::NO_ERRORS;
		//
		return iErrCode;
	}

public:

	/*!	\brief Статическая функция активации библиотеки сокетов WSA 
	*/
	static int activeSocketLib()
	{	
		WSADATA	srctWSA_l;
		return	WSAStartup ( MAKEWORD(2,2), &srctWSA_l );
	}

	/*!	\brief Статическая функция деактивации библиотеки сокетов WSA 
	*/
	static void deactiveSocketLib()
	{	WSACleanup(); }

	/*! \brief Описание ошибки 
		\param Код запрашиваемой ошибки
		\return Текстовая строка с описанием ошиибки
	*/
	static const std::string errorName( int iErrorCode_p ) //const
	{
		switch ( iErrorCode_p )
		{
		case NO_ERRORS:
			return "Ошибок нет";
		case INVALID_SOCKET_ERROR:
			return "Плохой сокет";
		case CAN_NOT_BIND_ERROR:
			return "Ошибка при попытке привязки адреса к сокету";
		case CONNECT_FUNCTION_ERROR:	
			return "Ошибка при попытке соединения";
		case SELECT_FUNCTION_ERROR:		
			return "Ошибка при вызве функции select";
		case TIME_OUT_HEPPENED:			
			return "Timeout";
		case RECIEVE_FUNCTION_ERROR:	
			return "Ошибка при получении данных";
		case SEND_FUNCTION_ERROR:		
			return "Ошибка при отправке данных";
		case OUT_QUEUE_EMPTY:			
			return "Очередь на отправку пуста";
		case ALREADY_CONNECTED:			
			return "Уже есть соединение";
		case ALREADY_LISTERNING:		
			return "Уже слушает";
		case NOT_CONNECTED:				
			return "Нет соединения";
		case SOCKET_CTRL_ERROR:			
			return "Ошибка при попытке перевода сокета в блокирующий режим";
		case ACCEPT_FUNCTION_ERROR:		
			return "Ошибка при попытке вызова функции accept";
		case SHUT_DOWN_ERROR:
			return "Ошибка закрытия сокета";
		}
		return "Неизвестный код ошибки";
	}

	/*!	\brief Конструктор
		\param ui16PortNumber_p - номер порта создаваемого сокета,
		\param uiIP_p - IP адрес создаваемого сокета.
	*/
	cTCPbase( unsigned short int ui16PortNumber_p = 0, unsigned int uiIP_p = 0 ) :
		uiBufferSize	( 1024 )
	{	iErrCode = prepareSocket( ui16PortNumber_p, uiIP_p ); }

	/*!	\brief Конструктор
		\param ui16PortNumber_p - номер порта создаваемого сокета,
		\param pIP_p - строка-адрес создаваемого сокета.
	*/
	cTCPbase( unsigned short int ui16PortNumber_p, const char* pIP_p ) :
		uiBufferSize	( 1024 )
	{	iErrCode = prepareSocket( ui16PortNumber_p, inet_addr( pIP_p ) ); }

	/*! \brief Код последней ошибки  
		\return Значение переменной utl::cTCPbase::iErrCode
	*/
	int errorCode() const
	{	return iErrCode; }

	/*! \brief Описание последней ошибки 
		\return Текстовая строка с описанием ошибки
	*/
	const std::string errorName( ) const
	{	return errorName( iErrCode ); }

	/*!	\brief Установить новый адрес сокета 
		\return Код ошибки ( значение переменной utl::cTCPbase::iErrCode )
	*/
	int setLocalAddress( unsigned short int ui16PortNumber_p, const char* pIP_p = "0.0.0.0" )
	{	
		closesocket( pWorkSocket );
		return ( iErrCode = prepareSocket( ui16PortNumber_p, inet_addr( pIP_p ) ) ); 
	}

	/*!	\brief Установить максимальный размер принимаемого за раз сообщения
	*/
	void setBufferSize( unsigned int uiBufferSize_p )
	{	uiBufferSize = uiBufferSize_p; }

	/*!	\brief Установить таймаут для функции select
	*/
	void setTimeOut( unsigned int uiMicroSecs_p )
	{	
		tTimeOut.tv_sec		= 0;
		tTimeOut.tv_usec	= uiMicroSecs_p;
	}

	virtual int dissconnect( )
	{	return iErrCode = !shutdown( pWorkSocket, 0x02 ) ? NO_ERRORS : SHUT_DOWN_ERROR; }

	/*!	\brief Добавить в очередь сообщение из массива 
		\param pData_p - указатель на кнстантный массив байт,
		\param szBufferSize_p - размер массива.
	*/
	void pushData( const char* pData_p, ::size_t szBufferSize_p )	
	{	que_strOut.push( std::string( pData_p, szBufferSize_p ) ); }

	/*!	\brief Забрать сообщение из входной очереди 
		\param strBuffer_p - ссылка на строку STL, в которую будет произведена запись
	*/
	void popData( std::string& strBuffer_p )
	{	
		if ( que_strIn.size() )
		{
			strBuffer_p = que_strIn.front();
			que_strIn.pop(); 
		}
	}

	/*!	\brief Размер входной очереди 
		\return Размер массива utl::cTCPbase::que_strIn
	*/
	size_t inDataSize() const
	{	return que_strIn.size(); }

	/*! \brief Очистить очередь входных сообщений
	*/
	void clearIn()
	{	while( que_strIn.size() ) que_strIn.pop(); }

	/*!	\brief Размер выходной очереди 
		\return Размер массива utl::cTCPbase::que_strOut
	*/
	size_t outDataSize() const
	{	return que_strOut.size(); }

	/*! \brief Очистить очередь выходных сообщений
	*/
	void clearOut()
	{	while( que_strOut.size() ) que_strOut.pop(); }

	/*!	\brief Виртуальный деструктор класса

		Закрывет сокет перед удалением объекта
	*/
	virtual ~cTCPbase()
	{	closesocket( pWorkSocket ); }
};

/*!	\brief Класс для создания клиента соединения.

Организует передачу данных по протоколам TCP/UDP.
*/
class cTCPclient : public cTCPbase
{
protected:
	bool bConnected; ///< Есть ли соединение

	int readSocket( const fd_set *p_strctReadScks_p )
	{
		// Если сокет готов к чтению производим его 
		if ( FD_ISSET( pWorkSocket, p_strctReadScks_p ) )
		{
			char* pBuffer_l = new char[uiBufferSize];
			int iBytesRecieved_l = recv( pWorkSocket, pBuffer_l, uiBufferSize, 0 );
			if ( iBytesRecieved_l > 0 )
				que_strIn.push( std::string( pBuffer_l, iBytesRecieved_l ) );
			else 
				bConnected	= false,
				iErrCode	= iBytesRecieved_l < 0 ? 
									eErrorTypes::RECIEVE_FUNCTION_ERROR :
										eErrorTypes::NOT_CONNECTED;
			delete [] pBuffer_l;
		}
		return iErrCode;
	}

	int writeSocket( const fd_set *p_strctWriteScks_p )
	{
		// Если вы ходная очередь пуста записываем код ошибки
		if ( !que_strOut.size() ) iErrCode =  eErrorTypes::OUT_QUEUE_EMPTY ;
		// Если сокет готов к передаче
		else if ( FD_ISSET( pWorkSocket, p_strctWriteScks_p ) )
		{
			int	iBytesRecieved_l = send (	pWorkSocket,	que_strOut.front().data(), 
											(int) que_strOut.front().size(), 0 );
			if ( iBytesRecieved_l > 0 )
				que_strOut.pop();
			else
				bConnected	= false,
				iErrCode	= iBytesRecieved_l < 0 ?
								eErrorTypes::SEND_FUNCTION_ERROR :
									eErrorTypes::NOT_CONNECTED;
		}
		return iErrCode;
	}

public:

	cTCPclient( unsigned short int ui16PortNumber_p = 0, unsigned int uiIP_p = 0 ) :
		cTCPbase	( ui16PortNumber_p, uiIP_p ),
		bConnected	( false )
	{ }

	cTCPclient( unsigned short int ui16PortNumber_p, const char* pIP_p ) :
		cTCPbase	( ui16PortNumber_p, pIP_p ),
		bConnected	( false )
	{ }

	bool connected( ) const
	{	return bConnected; }

	int connectTo ( unsigned short int ui16PortNumber_p, const char* pIP_p = "127.0.0.1" )
	{
		if ( !bConnected )
		{
			unsigned long	ulOnFlag_l	= IOC_IN;	
			sockaddr_in		strctRemoteAddr_l;
			strctRemoteAddr_l.sin_family			= AF_INET;
			strctRemoteAddr_l.sin_port				= htons( ui16PortNumber_p );
			strctRemoteAddr_l.sin_addr.S_un.S_addr	= inet_addr( pIP_p );
			// Перевод сокета в неблокирующий режим
			if ( ioctlsocket( pWorkSocket, FIONBIO, &ulOnFlag_l ) < 0 )
				//return ( iErrCode = eErrorTypes::SOCKET_CTRL_ERROR );
				iErrCode = eErrorTypes::SOCKET_CTRL_ERROR;
			int iConnectResult_l = connect ( pWorkSocket, (sockaddr *) &strctRemoteAddr_l, sizeof( sockaddr ) );
			// Перевод сокета в блокирующий режим
			if ( ioctlsocket( pWorkSocket, FIONBIO, &ulOnFlag_l ) < 0 )
				return ( iErrCode = eErrorTypes::SOCKET_CTRL_ERROR );
			bConnected = true;
			return ( iErrCode = eErrorTypes::NO_ERRORS );
		}
		else
			return ( iErrCode = eErrorTypes::ALREADY_CONNECTED );
	}

	int dissconnect()
	{
		bConnected = false;
		return cTCPbase::dissconnect();
	}

	int nextDataRead ( )
	{
		if ( !bConnected ) return ( iErrCode = eErrorTypes::NOT_CONNECTED );
		// Массивы сокетов на чтение, передачу и с ошибками
		fd_set	strctReadScks_l;
		// Чистка массивов
		FD_ZERO( &strctReadScks_l	);	
		// Добавление в массив на чтение слушающего сокета 
		FD_SET( pWorkSocket, &strctReadScks_l );
		// Запрос на чтение
		int iSelectResult_l = select ( 0, &strctReadScks_l, NULL, /*&strctErrorScks_l*/NULL, &tTimeOut );
		if ( iSelectResult_l )
		{
			// Пока ошибок нет :)
			iErrCode = eErrorTypes::NO_ERRORS;
			iErrCode = readSocket( &strctReadScks_l );
			/*
			// Если сокет готов к чтению производим его 
			if ( FD_ISSET( pWorkSocket, &strctReadScks_l ) )
			{
				char* pBuffer_l = new char[uiBufferSize];
				int iBytesRecieved_l = recv( pWorkSocket, pBuffer_l, uiBufferSize, 0 );
				if ( iBytesRecieved_l > 0 )
					que_strIn.push( std::string( pBuffer_l, iBytesRecieved_l ) );
				else 
				{	
					bConnected = false;
					if ( iBytesRecieved_l < 0 )
						iErrCode = eErrorTypes::RECIEVE_FUNCTION_ERROR;
					else
						iErrCode = eErrorTypes::NOT_CONNECTED;
				}
				delete [] pBuffer_l;
			}*/
		}
		else //if (  )
			iErrCode = iSelectResult_l < 0 ? 
							eErrorTypes::SELECT_FUNCTION_ERROR :	// Критическая ошибка 
								eErrorTypes::TIME_OUT_HEPPENED;		// Таймаут
		return iErrCode ;
	}

	int nextDataWrite ( )
	{
		if ( !bConnected ) return ( iErrCode = eErrorTypes::NOT_CONNECTED );
		// Проверка на наличие сообщений в очереди
		if ( !que_strOut.size() ) return ( iErrCode =  eErrorTypes::OUT_QUEUE_EMPTY );
		// Массивы сокетов на чтение, передачу и с ошибками
		fd_set	strctWriteScks_l;
		// ЗаЧистка массивов
		FD_ZERO( &strctWriteScks_l );
		// Добавление в массив на чтение слушающего сокета 
		FD_SET( pWorkSocket, &strctWriteScks_l );
		// Запрос на передачу
		int	iSelectResult_l = select ( 0, NULL, &strctWriteScks_l, NULL /*&strctErrorScks_l*/, &tTimeOut );
		if ( iSelectResult_l > 0 )
		{
			// Пока ошибок нет
			iErrCode = eErrorTypes::NO_ERRORS;
			/*
			// Если сокет готов к передаче
			if ( FD_ISSET( pWorkSocket, &strctWriteScks_l ) )
			{
				int	iBytesRecieved_l = send (	pWorkSocket,	que_strOut.front().data(), 
												(int) que_strOut.front().size(), 0 );
				if ( iBytesRecieved_l > 0 )
					que_strOut.pop();
				else
				{
					bConnected = false;
					if ( iBytesRecieved_l < 0 ) // Ошибка соединения отключаемся
						iErrCode = eErrorTypes::SEND_FUNCTION_ERROR;
					/*else
						iErrCode = eErrorTypes::NOT_CONNECTED;*//*
				}
			}*/
			iErrCode = writeSocket( &strctWriteScks_l );
		}
		else 
			iErrCode = iSelectResult_l < 0 ? 
							eErrorTypes::SELECT_FUNCTION_ERROR :	// Критическая ошибка 
								eErrorTypes::TIME_OUT_HEPPENED;		// Таймаут
		/*	
		if ( iSelectResult_l < 0 )
			// Критическая ошибка
			iErrCode = eErrorTypes::SELECT_FUNCTION_ERROR;
		else
			iErrCode = eErrorTypes::TIME_OUT_HEPPENED;*/
		return iErrCode;
	}

	int nextDataChange ( )
	{
		// Проверка наличия соединения
		if ( !bConnected ) return ( iErrCode = eErrorTypes::NOT_CONNECTED );
		// Массивы на чтение и передачу
		fd_set	strctReadScks_l,
				strctWriteScks_l;
		// ЗаЧистка массивов
		FD_ZERO( &strctReadScks_l );
		FD_ZERO( &strctWriteScks_l );
		// Добавление сокета в массивы на чтение и запись
		FD_SET( pWorkSocket, &strctReadScks_l );
		FD_SET( pWorkSocket, &strctWriteScks_l );
		int	iSelectResult_l = select ( 0, &strctReadScks_l, &strctWriteScks_l, NULL /*&strctErrorScks_l*/, &tTimeOut );
		if ( iSelectResult_l > 0 )
		{
			// Пока ошибок нет
			iErrCode = eErrorTypes::NO_ERRORS;
			// Если вы ходная очередь пуста записываем код ошибки
			iErrCode = que_strOut.size() ? 
							writeSocket( &strctWriteScks_l ) : 
									eErrorTypes::OUT_QUEUE_EMPTY;
			iErrCode = readSocket( &strctReadScks_l );
			/*
			// Если сокет готов к передаче
			else if ( FD_ISSET( pWorkSocket, &strctWriteScks_l ) )
			{
				int	iBytesRecieved_l = send (	pWorkSocket,	que_strOut.front().data(), 
												(int) que_strOut.front().size(), 0 );
				if ( iBytesRecieved_l > 0 )
					que_strOut.pop();
				else
				{
					bConnected = false;
					iErrCode = iBytesRecieved_l < 0 ?
									eErrorTypes::SEND_FUNCTION_ERROR :
										eErrorTypes::NOT_CONNECTED;
				}
			}
			// Если сокет готов к чтению производим его 
			if ( FD_ISSET( pWorkSocket, &strctReadScks_l ) )
			{
				char* pBuffer_l = new char[uiBufferSize];
				int iBytesRecieved_l = recv( pWorkSocket, pBuffer_l, uiBufferSize, 0 );
				if ( iBytesRecieved_l > 0 )
					que_strIn.push( std::string( pBuffer_l, iBytesRecieved_l ) );
				else 
				{	
					bConnected = false;
					iErrCode = iBytesRecieved_l < 0 ? 
									eErrorTypes::RECIEVE_FUNCTION_ERROR :
										eErrorTypes::NOT_CONNECTED;
				}
				delete [] pBuffer_l;
			}
			*/
		}
		else 
			iErrCode = iSelectResult_l < 0 ? 
							eErrorTypes::SELECT_FUNCTION_ERROR :	// Критическая ошибка 
								eErrorTypes::TIME_OUT_HEPPENED;		// Таймаут
		/*
		if ( iSelectResult_l < 0 )
			// Критическая ошибка
			iErrCode = eErrorTypes::SELECT_FUNCTION_ERROR;
		else
			// Ничего не пришло и некому отправлять
			iErrCode = eErrorTypes::TIME_OUT_HEPPENED;*/
		return iErrCode;
	}
};

/*!	\brief Класс для создания сервера.

Организует передачу данных по протоколам TCP/UDP.
*/
class cTCPserver : public cTCPbase
{
protected:
	bool						bIsListernig;	///< Включен ли режим ожидания 
	std::vector<cClientData>	vClientsList;	///< Список клиентов

	int addNewClient()
	{
		cClientData		strctNewSocket_l;
		int				iAddrLen_l = sizeof ( sockaddr_in );
		unsigned long	ulOnFlag_l	= IOC_IN;	
		// Перевод сокета в неблокирующий режим
		if ( ioctlsocket( pWorkSocket, FIONBIO, &ulOnFlag_l ) < 0 )
			return ( iErrCode = eErrorTypes::SOCKET_CTRL_ERROR );
		// Закрепление соединения за сокетом клиента
		strctNewSocket_l.pSocket =  accept(	pWorkSocket, 
											(sockaddr*) &(strctNewSocket_l.strctAddr), 
											&iAddrLen_l );
		// Проверка на возможные ошибки
		if (	INVALID_SOCKET == strctNewSocket_l.pSocket
			 || SOCKET_ERROR == strctNewSocket_l.pSocket )
			return ( iErrCode = eErrorTypes::ACCEPT_FUNCTION_ERROR );
		// Добавление сокета в массив 
		vClientsList.push_back( strctNewSocket_l );
		// Отключение режима неблокирующего сокета
		if ( ioctlsocket( pWorkSocket, FIONBIO, &ulOnFlag_l ) < 0 )
			return ( iErrCode = eErrorTypes::SOCKET_CTRL_ERROR );
		return ( iErrCode = eErrorTypes::NO_ERRORS );
	}

	int dellClient( size_t szClientNumber_p )
	{
		closesocket( vClientsList[szClientNumber_p].pSocket );
		vClientsList.erase( &(vClientsList[szClientNumber_p]) );
		return eErrorTypes::NO_ERRORS;
	}

	int readScks( const fd_set *p_strctReadScks_p )
	{
		int		iBytesRecieved_l	= 0;
		char*	pBuffer_l			= new char[uiBufferSize];
		for ( size_t i = 0; i < vClientsList.size(); )
		{
			// Если сокет готов к чтению производим его 
			if ( FD_ISSET( (vClientsList[i].pSocket), p_strctReadScks_p ) )
			{
				iBytesRecieved_l = recv( vClientsList[i].pSocket, pBuffer_l, uiBufferSize, 0 );
				// Сколько байт получено?
				if ( iBytesRecieved_l > 0 )
					que_strIn.push( std::string( pBuffer_l, iBytesRecieved_l ) );
				else
				{	// Ошибка соединения, отключаем сокет
					dellClient( i );
					iErrCode = iBytesRecieved_l < 0 ?
									eErrorTypes::RECIEVE_FUNCTION_ERROR :
										eErrorTypes::NOT_CONNECTED;
					/*
					if ( iBytesRecieved_l < 0 ) 
						iErrCode = eErrorTypes::RECIEVE_FUNCTION_ERROR;
					else
						iErrCode = eErrorTypes::NOT_CONNECTED;*/
					continue;
				}
			}
			++i;
		}
		delete [] pBuffer_l;
		return iErrCode;
	}

	int writeScks( const fd_set *p_strctWriteScks_p  )
	{
		int	iBytesRecieved_l = 0;
		for ( size_t i = 0; i < vClientsList.size(); )
		{
			// Если сокет готов к передаче
			if ( FD_ISSET( vClientsList[i].pSocket, p_strctWriteScks_p ) )
			{
				iBytesRecieved_l = send (	vClientsList[i].pSocket,	que_strOut.front().data(), 
											(int) que_strOut.front().size(),	0	);
				// Ошибка соединения, отключаем сокет
				if ( iBytesRecieved_l <= 0 ) 
				{
					iErrCode = eErrorTypes::SEND_FUNCTION_ERROR;
					dellClient( i );
					continue;
				}
			}
			++i;
		}
		if ( iBytesRecieved_l > 0 ) que_strOut.pop();
		return iErrCode;
	}

public:

	cTCPserver( unsigned short int ui16PortNumber_p = 0, unsigned int uiIP_p = 0 ) :
		cTCPbase		( ui16PortNumber_p, uiIP_p ),
		bIsListernig	( false	)
	{ }

	cTCPserver( unsigned short int ui16PortNumber_p, const char* pIP_p ) :
		cTCPbase		( ui16PortNumber_p, pIP_p ),
		bIsListernig	( false	)
	{ }

	size_t clientsNumber() const
	{	return vClientsList.size(); }
	
	void startListen ( int iBackLog )
	{
		if ( !bIsListernig )
		{
			bIsListernig = true;
			listen ( pWorkSocket, iBackLog );
			iErrCode = eErrorTypes::NO_ERRORS;
		}
		else
			iErrCode = eErrorTypes::ALREADY_LISTERNING;
	}

	int nextDataRead ( )
	{
		// Массивы сокетов на чтение, передачу и с ошибками
		fd_set	strctReadScks_l;
		// Пока ошибок нет
		iErrCode = eErrorTypes::NO_ERRORS;
		// ЗаЧистка массивов
		FD_ZERO( &strctReadScks_l );	
		// Добавление в массив на чтение слушающего сокета 
		FD_SET( pWorkSocket, &strctReadScks_l );
		// Распределение сокетов по массивам чтения
		for ( size_t i = 0; i < vClientsList.size(); i++ )
			if ( vClientsList[i].iDataDirection | cClientData::READ_DATA )
				FD_SET( ( vClientsList[i].pSocket ), &strctReadScks_l );
		// Запрос на чтение
		int iSelectResult_l = select ( 0, &strctReadScks_l, NULL, /*&strctErrorScks_l*/NULL, &tTimeOut );
		if ( iSelectResult_l > 0 )
		{
			// Появилось новое соединение
			if ( FD_ISSET( pWorkSocket, &strctReadScks_l ) ) addNewClient();
			// Чтение данных
			iErrCode = readScks( &strctReadScks_l );
			/*
			int		iBytesRecieved_l	= 0;
			char*	pBuffer_l			= new char[uiBufferSize];
			for ( size_t i = 0; i < vClientsList.size(); )
			{
				// Если сокет готов к чтению производим его 
				if ( FD_ISSET( (vClientsList[i].pSocket), &strctReadScks_l ) )
				{
					iBytesRecieved_l = recv( vClientsList[i].pSocket, pBuffer_l, uiBufferSize, 0 );
					// Сколько байт получено?
					if ( iBytesRecieved_l > 0 )
						que_strIn.push( std::string( pBuffer_l, iBytesRecieved_l ) );
					else
					{	// Ошибка соединения, отключаем сокет
						dellClient( i );
						if ( iBytesRecieved_l < 0 ) 
							iErrCode = eErrorTypes::RECIEVE_FUNCTION_ERROR;
						else
							iErrCode = eErrorTypes::NOT_CONNECTED;
						continue;
					}
				}
				i++;
			}
			delete [] pBuffer_l;
			*/
		}
		else
			iErrCode =	iSelectResult_l < 0 ? 
							eErrorTypes::SELECT_FUNCTION_ERROR :
								eErrorTypes::TIME_OUT_HEPPENED;
		/*	if ( iSelectResult_l < 0 )
			// Критическая ошибка
			iErrCode = eErrorTypes::SELECT_FUNCTION_ERROR;
		else 
			// Таймаут
			iErrCode = eErrorTypes::TIME_OUT_HEPPENED;*/
		return iErrCode;
	}

	int nextDataWrite ( )
	{
		// Массивы сокетов на чтение, передачу и с ошибками
		fd_set	strctReadScks_l, 
				strctWriteScks_l;
		// Пока ошибок нет
		iErrCode = eErrorTypes::NO_ERRORS;
		// ЗаЧистка массивов
		FD_ZERO( &strctReadScks_l	);	
		FD_ZERO( &strctWriteScks_l	);
		// Добавление в массив на чтение слушающего сокета 
		FD_SET( pWorkSocket, &strctReadScks_l );
		// Распределение сокетов по массивам чтения и передачи
		if ( que_strOut.size() )
			for ( size_t i = 0; i < vClientsList.size(); i++ )
				if ( vClientsList[i].iDataDirection | cClientData::WRITE_DATA )
					FD_SET( vClientsList[i].pSocket, &strctWriteScks_l );
		// Запрос на передачу
		int iSelectResult_l = select ( 0, &strctReadScks_l, &strctWriteScks_l, NULL /*&strctErrorScks_l*/, &tTimeOut );
		if ( iSelectResult_l > 0 )
		{
			// Появилось новое соединение
			if ( FD_ISSET( pWorkSocket, &strctReadScks_l ) ) addNewClient();
			iErrCode =	que_strOut.size() ? 
							writeScks( &strctWriteScks_l ) : 
								eErrorTypes::OUT_QUEUE_EMPTY;
			/*
			if ( que_strOut.size() )
			{
				int	iBytesRecieved_l = 0;
				for ( size_t i = 0; i < vClientsList.size(); )
				{
					// Если сокет готов к передаче
					if ( FD_ISSET( vClientsList[i].pSocket, &strctWriteScks_l ) )
					{
						iBytesRecieved_l = send (	vClientsList[i].pSocket,	que_strOut.front().data(), 
													(int) que_strOut.front().size(),	0	);
						// Ошибка соединения, отключаем сокет
						if ( iBytesRecieved_l < 0 ) 
						{
							iErrCode = eErrorTypes::SEND_FUNCTION_ERROR;
							dellClient( i );
							continue;
						}
					}
					i++;
				}
				if ( iBytesRecieved_l > 0 ) que_strOut.pop();
			}
			else
				iErrCode = eErrorTypes::OUT_QUEUE_EMPTY; */
		}
		else 
			iErrCode =	iSelectResult_l < 0 ?
							eErrorTypes::SELECT_FUNCTION_ERROR :
								iErrCode = eErrorTypes::TIME_OUT_HEPPENED;
		/*if ( iSelectResult_l < 0 )
			return eErrorTypes::SELECT_FUNCTION_ERROR;
		else
			iErrCode = eErrorTypes::TIME_OUT_HEPPENED;*/
		return iErrCode;
	}

	int nextDataChange( )
	{
		// Массивы сокетов на чтение, передачу и с ошибками
		fd_set	strctReadScks_l, 
				strctWriteScks_l;
		// ЗаЧистка массивов
		FD_ZERO( &strctReadScks_l	);	
		FD_ZERO( &strctWriteScks_l	);
		// Добавление в массив на чтение слушающего сокета 
		FD_SET( pWorkSocket, &strctReadScks_l );
		// Распределение сокетов по массивам чтения и передачи
		for ( size_t i = 0; i < vClientsList.size(); ++i )
		{
			if ( que_strOut.size() && ( vClientsList[i].iDataDirection | cClientData::WRITE_DATA ) )
				FD_SET( vClientsList[i].pSocket, &strctWriteScks_l );
			if ( vClientsList[i].iDataDirection | cClientData::READ_DATA )
				FD_SET( vClientsList[i].pSocket, &strctReadScks_l );
		}		
		// Запрос на чтение и передачу
		int iSelectResult_l = select ( 0, &strctReadScks_l, &strctWriteScks_l, NULL /*&strctErrorScks_l*/, &tTimeOut );
		if ( iSelectResult_l > 0 )
		{
			// Пока ошибок нет
			iErrCode = eErrorTypes::NO_ERRORS;
			// Появилось новое соединение
			if ( FD_ISSET( pWorkSocket, &strctReadScks_l ) ) addNewClient();
			iErrCode =	que_strOut.size() ? 
							writeScks( &strctWriteScks_l ) : 
								eErrorTypes::OUT_QUEUE_EMPTY;
			iErrCode =	readScks( &strctReadScks_l );
		}		
		else
			iErrCode =	iSelectResult_l < 0 ?
							eErrorTypes::SELECT_FUNCTION_ERROR :
								eErrorTypes::TIME_OUT_HEPPENED;
		/*if ( iSelectResult_l < 0 )
			return eErrorTypes::SELECT_FUNCTION_ERROR;
		else
			iErrCode = eErrorTypes::TIME_OUT_HEPPENED;*/
		return iErrCode;
	}

	~cTCPserver()
	{
		while( vClientsList.size() ) dellClient( 0 );
		closesocket( pWorkSocket );
	}
};
} // namespace utl
// ------------------------------------------------------------------
#endif
// ------------------------------------------------------------------


