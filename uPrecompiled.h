// ---------------------------------------------------------------------------------------------------------
#ifndef UTL_CHECKERS_H_MRV
#define UTL_CHECKERS_H_MRV
// ---------------------------------------------------------------------------------------------------------
/*!   \file		   uPrecompiled.h
      \author	   Ruslan V. Maltsev ( mrv.work.box@yandex.ru )
      \date		   29.06.2012
      \version	   0.1
      \brief	   Файл содержайщий конструкции обобщённого программирования, .
      \copyright  GNU Public License

   Добавить:
   <ol>
      <li> [create] </li>
   </ol>
*/
// ---------------------------------------------------------------------------------------------------------
#include <cstddef>
//
namespace utl {
namespace assert {

template< bool expression_value > class correct; 
template<> class correct< true > { }; 

namespace type {

template< class checking_type > class sign; 
template<> class sign< char >        { }; 
template<> class sign< int >         { }; 
template<> class sign< short int >   { }; 
template<> class sign< long int >    { }; 
#if defined ( WIN32 ) || defined ( WINCE )
template<> class sign< __int64 >     { }; 
#endif // defined ( WIN32 ) || defined ( WINCE )

template< class checking_type > class unsign; 
template<> class unsign< unsigned char >         { }; 
template<> class unsign< unsigned int >          { }; 
template<> class unsign< unsigned short int >    { }; 
template<> class unsign< unsigned long int >     { }; 
#if defined ( WIN32 ) || defined ( WINCE )
template<> class unsign< unsigned __int64 >      { }; 
#endif // defined ( WIN32 ) || defined ( WINCE )

template< class checking_type > class float_point; 
template<> class float_point< float >         { }; 
template<> class float_point< double >        { }; 
template<> class float_point< long double >   { }; 

template< class checking_type > class integer; 
// signed
template<> class integer< char >       { }; 
template<> class integer< int >        { }; 
template<> class integer< short int >  { }; 
template<> class integer< long int >   { }; 
#if defined ( WIN32 ) || defined ( WINCE )
template<> class integer< __int64 >    { }; 
#endif // defined ( WIN32 ) || defined ( WINCE )
// unsigned
template<> class integer< unsigned char >       { }; 
template<> class integer< unsigned int >        { }; 
template<> class integer< unsigned short int >  { }; 
template<> class integer< unsigned long int >   { }; 
#if defined ( WIN32 ) || defined ( WINCE )
template<> class integer< unsigned __int64 >    { }; 
#endif // defined ( WIN32 ) || defined ( WINCE )

template< class checking_type > class numeric; 
// signed
template<> class numeric< char >       { }; 
template<> class numeric< int >        { }; 
template<> class numeric< short int >  { }; 
template<> class numeric< long int >   { }; 
#if defined ( WIN32 ) || defined ( WINCE )
template<> class numeric< __int64 >    { }; 
#endif // defined ( WIN32 ) || defined ( WINCE )
// unsgined
template<> class numeric< unsigned char >       { }; 
template<> class numeric< unsigned int >        { }; 
template<> class numeric< unsigned short int >  { }; 
template<> class numeric< unsigned long int >   { }; 
#if defined ( WIN32 ) || defined ( WINCE )
template<> class numeric< unsigned __int64 >    { }; 
#endif // defined ( WIN32 ) || defined ( WINCE )
// float
template<> class numeric< float >         { }; 
template<> class numeric< double >        { }; 
template<> class numeric< long double >   { }; 
}; // namespace type {
}; // namespace check {

namespace compute {
template< std::size_t numeric_value >
class bytes {
private:
   template< std::size_t rest_value >
   struct next_iteration { enum { size = next_iteration< rest_value / 0x100 >::size + 1 }; };

   template<> struct next_iteration< 0 > { enum { size = 0 }; };
public:
   enum { result = next_iteration< numeric_value >::size };
}; // class bytes {
}; // namespace compute {
}; // namespace utl {
#endif // UTL_CHECKERS_H_MRV