// ---------------------------------------------------------------------------------------------------------
#ifndef UTL_MUTEX_H_MRV
#define UTL_MUTEX_H_MRV
// ---------------------------------------------------------------------------------------------------------
/*!
   \file		   uMutex.h
   \author	   Ruslan V. Maltsev ( mrv.work.box@yandex.ru )
   \date		   10.11.2010
   \version	   0.3.2
   \brief	   Класс-реализация платформонезависимого мьютекса.
   \copyright  GNU Public License

   Добавить:
   <ol>
      <li> [create] Проверка исключений </li>
   </ol>
*/
// ---------------------------------------------------------------------------------------------------------
#include <vector>
#include <map>
// ---------------------------------------------------------------------------------------------------------
#include <assert.h>
// ---------------------------------------------------------------------------------------------------------
#if defined ( WIN32 ) || defined ( WINCE )
#include <windows.h>
#else
#include <stdlib.h>
#include <pthread.h>
#endif   
// ---------------------------------------------------------------------------------------------------------
#ifndef NULL 
#define NULL 0
#endif
// ---------------------------------------------------------------------------------------------------------
namespace utl {
// ---------------------------------------------------------------------------------------------------------
#if defined ( WIN32 ) || defined( WINCE )
   typedef CRITICAL_SECTION mutex_realization;
#else    // WIN32 || WINCE 
   typedef pthread_mutex_t mutex_realization;
#endif
// ---------------------------------------------------------------------------------------------------------

class mutex
{
private:
   bool              isLocked_m;    ///< Флаг блокировки
   mutex_realization mutex_m;       ///< Реализация мьютекса
   // ------------------------------------------------------------------------------------------------------

   void init ()
   {
      #if defined ( WIN32 ) || defined ( WINCE )
         InitializeCriticalSection( &mutex_m );
      #else
         pthread_mutex_init( &mutex_m, NULL );
      #endif
   }
   // ------------------------------------------------------------------------------------------------------
public:

   mutex () : isLocked_m( false ), mutex_m() { init(); }
   // ------------------------------------------------------------------------------------------------------

   /*!	\brief Конструктор копирования.

      Необходим для использовиня данного класса в качестве элемнтов контейнеров типа std::vector и std::map. 
      При вызове данного контруктора как такового копирования мутекса не происходит. Сохраняется только 
      состтояние mutex::isLocked_m.
   */
   mutex ( const mutex & etalon_cp ) : isLocked_m( etalon_cp.isLocked_m ), mutex_m() { init(); }
   // ------------------------------------------------------------------------------------------------------

   ~ mutex () 
   {  
      #if defined ( WIN32 ) || defined ( WINCE )
         DeleteCriticalSection( &mutex_m );
      #else
         pthread_mutex_destroy( & mutex_m );
      #endif
   }
   // ------------------------------------------------------------------------------------------------------

   bool isLocked () const { return isLocked_m; }
   // ------------------------------------------------------------------------------------------------------

   void lock ()
   {
      #if defined ( WIN32 ) || defined ( WINCE )
         EnterCriticalSection( &mutex_m );
      #else
         pthread_mutex_lock( & mutex_m );
      #endif
      isLocked_m = true;
   }
   // ------------------------------------------------------------------------------------------------------

   void unlock ()
   {
      isLocked_m = false;
      #if defined ( WIN32 ) || defined ( WINCE )
         LeaveCriticalSection( &mutex_m );
      #else
         pthread_mutex_unlock( & mutex_m );
      #endif
   }
   // ------------------------------------------------------------------------------------------------------
}; // class mutex
// ---------------------------------------------------------------------------------------------------------

template < bool isLocketResult >
class dummyMutex
{
public:
   dummyMutex () { ; }
   dummyMutex ( const dummyMutex & ) { ; }
   bool isLocked () const { return isLocketResult; }
   void lock () { ; }
   void unlock () { ; }
};
// ---------------------------------------------------------------------------------------------------------

template< class mutex_class = mutex >
class locker
{
private:
   mutex_class & mutex_m;
   // ------------------------------------------------------------------------------------------------------
public:
   locker ( mutex_class & mutex_p ) : mutex_m( mutex_p ) { mutex_m.lock(); }
   // ------------------------------------------------------------------------------------------------------

   ~locker () { mutex_m.unlock(); }
   // ------------------------------------------------------------------------------------------------------
}; // class locker
// ---------------------------------------------------------------------------------------------------------

class mutexPool
{
public:
   enum results
   {
      RESULT_UNKNOWN          = 0x0,
      RESULT_OK               = 0x1,
      RESULT_NOT_EXIST        = 0x3,
      RESULT_ALREADY_EXIST    = 0x4,
      RESULT_ALREADY_DELETED  = 0x5
   };
   // ------------------------------------------------------------------------------------------------------
private:
   enum operations
   {
      CREATE   = 0x0,
      ERASE    = 0x1,
      OTHERS   = 0x2
   };
   // ------------------------------------------------------------------------------------------------------
   typedef std::map< std::size_t, mutex * >  subpool_map;
   typedef std::vector< subpool_map >        pool_vector;
   typedef std::vector< mutex >              mutexes_vector;
   // ------------------------------------------------------------------------------------------------------
   std::vector< mutex > mutexes_m;
   pool_vector subpools_m;
   // ------------------------------------------------------------------------------------------------------

   mutex * getMutex ( std::size_t mutexID_p, operations operation_p )
   {
      const std::size_t subpoolID_cm = mutexID_p % mutexes_m.size();
      locker<> locker_l( mutexes_m[ subpoolID_cm ] );
      subpool_map::iterator found_l = CREATE == operation_p  
                                          ? subpools_m[ subpoolID_cm ].lower_bound( mutexID_p ) 
                                          : subpools_m[ subpoolID_cm ].find( mutexID_p );
      mutex * result_l = NULL;
      if ( CREATE == operation_p )
      {
         if ( subpools_m[ subpoolID_cm ].end() == found_l || found_l->first != subpoolID_cm )
            result_l = subpools_m[ subpoolID_cm ].
               insert( found_l, std::make_pair( mutexID_p, new mutex() ) )->second;
         else assert( !"[ERROR] Mutex with this ID already exists in pool" );
      }
      else if ( subpools_m[ subpoolID_cm ].end() != found_l ) 
      {
         result_l = found_l->second;
         if ( ERASE == operation_p ) subpools_m[ subpoolID_cm ].erase( found_l );
      }
      return result_l;
   }
   // ------------------------------------------------------------------------------------------------------
public:
   mutexPool ( std::size_t subpoolsSize_p ) : 
      mutexes_m   ( subpoolsSize_p ), 
      subpools_m  ( subpoolsSize_p ) 
   {  assert( mutexes_m.size() ); }
   // ------------------------------------------------------------------------------------------------------

   results create ( std::size_t id_p )
   {  return NULL != getMutex( id_p, CREATE ) ? RESULT_OK : RESULT_ALREADY_EXIST; }
   // ------------------------------------------------------------------------------------------------------

   results lock ( std::size_t id_p )
   {
      mutex * const mutex_l = getMutex( id_p, OTHERS );
      if ( NULL == mutex_l ) return RESULT_NOT_EXIST;
      mutex_l->lock();
      return RESULT_OK;
   }
   // ------------------------------------------------------------------------------------------------------

   results unlock ( std::size_t id_p )
   {
      mutex * const mutex_l = getMutex( id_p, OTHERS );
      if ( NULL == mutex_l ) return RESULT_NOT_EXIST;
      assert( mutex_l->isLocked() );
      mutex_l->unlock();
      return RESULT_OK;
   }
   // ------------------------------------------------------------------------------------------------------

   results remove ( std::size_t id_p )
   {
      mutex * const mutex_l = getMutex( id_p, ERASE );
      if ( NULL == mutex_l ) return RESULT_NOT_EXIST;
      assert( mutex_l->isLocked() );
      mutex_l->unlock();
      delete mutex_l;
      return RESULT_OK;
   }
   // ------------------------------------------------------------------------------------------------------
}; // class mutexPull
// ---------------------------------------------------------------------------------------------------------
} // namespace utl {
// ---------------------------------------------------------------------------------------------------------
#endif // UTL_MUTEX_H_MRV
// ---------------------------------------------------------------------------------------------------------

