// -----------------------------------------------------------------------------------
#ifndef SMART_DATA_TEMPLATE_H_MRV
#define SMART_DATA_TEMPLATE_H_MRV
// -----------------------------------------------------------------------------------
/*!	\file		varType.h
	\author		MRV
	\date		29.12.2006
	\version	0.1
	\brief		����� ��� �������� ����� ������������� ����.
*/
// -----------------------------------------------------------------------------------
#include <string>
// -----------------------------------------------------------------------------------

/// ������������ ���� ���������� UTL (Useful Trifles Library)
namespace   utl {
// -----------------------------------------------------------------------------------
typedef __int64				int64_t;
typedef unsigned __int64	uint64_t;
typedef uint64_t			id_t;
typedef uint64_t			datetime_t;
// -----------------------------------------------------------------------------------

/*!	\brief ����� ��� �������� ������ ������������� ����
	
	...
*/
class cVarType
{
protected:
	unsigned int	uiState;	/*!< ������� ��������� �������. �������� 
									 ��� �������� ������ � ����� ������. */
	std::string		strData;	///< �������� ��� �������� ����������.

public:
	/// �������������� ����� ������
	enum enumDataTypes
	{
		UNKNOWN_T		= 0x00,	///< ����������� ��� ������
		BYTE_ARRAY_T	= 0x01,	///< ������ ���� ������������� �������
		STRING_T		= 0x02,	///< ASCII ������
		WSTRING_T		= 0x02,	///< UNICODE ������
	
		CHAR_T			= 0x10,	///< ����� ���� �� ������
		UCHAR_T			= 0x12,	///< ����� ���� ��� �����

		INT_T			= 0x20,	///< ����� �� ������
		UINT_T			= 0x21,	///< ����� ��� �����
		
		INT64_T			= 0x40,	///< 64-������ ����� �� ������
		UINT64_T		= 0x41,	///< 64-������ ����� ��� ������
		
		FLOAT_T			= 0x80,	///< ����� � ��������� ������ (double)
	};
	
	/// ������ ������ �������
	enum enumModes
	{
		CONVERSION_OFF	= 0x10000,	///< ��� ������ ������� ��������������
		CONVERSION_ON	= 0x20000	///< C ������� ������� ��������������
	};

    /*!	\brief ����������� �� ��������� */
	cVarType() : uiState (CONVERSION_OFF) {}
	// -------------------------------------------------------------------------------

    /*!	\brief ����������� ����������� ������������ ���� 
		\param varEtalon_p - ������������� ��������
	*/
	cVarType(const cVarType& varEtalon_p) : 
		uiState (CONVERSION_OFF) 
	{ this->operator =(varEtalon_p); }
	// -------------------------------------------------------------------------------

    /*!	\brief ����������� ����������� � ���� char 
		\param cEtalon_p - ������������� ��������
	*/
	cVarType(char cEtalon_p) : 
		uiState (CONVERSION_OFF + CHAR_T) 
	{ strData = cEtalon_p; }
	// -------------------------------------------------------------------------------

	/*!	\brief ����������� ����������� � ���� unsigned char 
		\param ucEtalon_p - ������������� ��������
	*/
	cVarType(unsigned char ucEtalon_p): 
		uiState (CONVERSION_OFF + CHAR_T) 
	{ strData = ucEtalon_p; }
	// -------------------------------------------------------------------------------

	/*!	\brief ����������� ����������� � ���� int 
		\param iEtalon_p - ������������� ��������
	*/
	cVarType(int iEtalon_p): 
		uiState (CONVERSION_OFF + INT_T) 
	{ strData.assign(&iEtalon_p, sizeof(int)); }
	// -------------------------------------------------------------------------------

	/*!	\brief ����������� ����������� � ���� unsigned int 
		\param uiEtalon_p - ������������� ��������
	*/
	cVarType(unsigned int uiEtalon_p) : 
		uiState (CONVERSION_OFF + UINT_T) 
	{ strData.assign(&iEtalon_p, sizeof(unsigned int)); }
	// -------------------------------------------------------------------------------

	/*!	\brief ����������� ����������� � ���� 64-���������� ������ 
		\param i64Etalon_p - ������������� ��������
	*/
	cVarType(int64_t i64Etalon_p) : 
		uiState (CONVERSION_OFF + INT64_T) 
	{ strData.assign(&iEtalon_p, sizeof(int64_t)); }
	// -------------------------------------------------------------------------------

	/*!	\brief ����������� ����������� � ���� 64-���������� ������������ ������ 
		\param ui64Etalon_p - ������������� ��������
	*/
	cVarType(uint64_t ui64Etalon_p) : 
		uiState (CONVERSION_OFF + UINT64_T) 
	{ strData.assign(&iEtalon_p, sizeof(uint64_t)); }
	// -------------------------------------------------------------------------------

	/*!	\brief ����������� ����������� � ���� std::string 
		\param strEtalon_p - ������������� ��������
	*/
	cVarType(const std::string& strEtalon_p) : 
		uiState (CONVERSION_OFF + STRING_T) 
	{ strData = strEtalon_p; }
	// -------------------------------------------------------------------------------

	/*!	\brief ����������� ����������� � ���� std::wstring 
		\param wstrEtalon_p - ������������� ��������
	*/
	cVarType(const std::wstring& wstrEtalon_p) : 
		uiState (CONVERSION_OFF + WSTRING_T) 
	{ strData.assign((char*)wstrEtalon_p.data(), wstrEtalon_p.lenght()<<1 ); }
	// -------------------------------------------------------------------------------

	/*!	\brief ����������� � ����������� ������� �� ������� ������������ ����� 
		\param pEtalon_p - ��������� �� ������ ������������ �����
		\param uiSize_p	- ������ ������� � ������
	*/
	cVarType(void* pEtalon_p, unsigned int uiSize_p) : 
		uiState (CONVERSION_OFF + BYTE_ARRAY_T) 
	{ strData.assign((char*)pEtalon_p, uiSize_p ); }
	// -------------------------------------------------------------------------------

	/*!	\brief �������� ������������
		\param varEtalon_p - ������������� ��������
		\return ������ �� ��������� ������ ������ ����� ���������� �������� ������������
	*/
	virtual cVarType& operator=(const cVarType& varEtalon_p)
	{ 
		uiState = varEtalon_p.uiState;
		strData = varEtalon_p.strData;
	}	
	// -------------------------------------------------------------------------------

	/*!	\brief �������� ���������
		\param varEtalon_p - �������� ��� ���������
		\return 
				- \b true - ���� ������ ���������,
				- \b false - ���� ������ ��������.
	*/
	virtual bool operator==(const cVarType& varEtalon_p)
	{ return strData == varEtalon_p.strData; }
};
// -----------------------------------------------------------------------------------
} // namespace   mtl {
// -----------------------------------------------------------------------------------
#endif
// -----------------------------------------------------------------------------------