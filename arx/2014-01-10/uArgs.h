// ---------------------------------------------------------------------------------------------------------	
#ifndef UTL_ARGS_H_MRV
#define UTL_ARGS_H_MRV
// ---------------------------------------------------------------------------------------------------------
#include <map>
#include <set>
#include <list>
#include <string>
#include <vector>
#include <cassert>
// ---------------------------------------------------------------------------------------------------------
#include <uTypes.h>
// ---------------------------------------------------------------------------------------------------------
namespace utl {
// ---------------------------------------------------------------------------------------------------------

class argument_definition {
private:
   bool           hasValue_m;    ///< ����� �� �������� �������� ��� �������� ������
   std::size_t    id_m;          ///< ������������� ��� �������� �������� ��������� ���������
   std::string    description_m, ///< �������� ��� ������������ ������ ���������
                  value_m; 
   strings_list   stringIDs_m;
public:
   argument_definition (   const strings_list & stringIDs_p, std::size_t id_p, 
                           const std::string & description_p, bool hasValue_p = false, 
                           const std::string & value_p = std::string() ) : 
      hasValue_m( hasValue_p ), id_m( id_p ), description_m( description_p ), value_m( value_p ), 
      stringIDs_m( stringIDs_p )
   { ; }
   argument_definition ( const argument_definition & etalon_p ) : 
      hasValue_m( etalon_p.hasValue_m ), id_m( etalon_p.id_m ), description_m( etalon_p.description_m ), 
      value_m( etalon_p.value_m ), stringIDs_m( etalon_p.stringIDs_m )
   { ; }
   //
   bool hasValue () const { return hasValue_m; }
   std::size_t id () const { return id_m; }
   const std::string & description () const { return description_m; }
   const std::string & value () const { return value_m; }
   const strings_list & stringIDs () const { return stringIDs_m; }
   //
   void setValue ( const std::string & value_p ) { value_m = value_p; }
   //
   const argument_definition & operator = ( const argument_definition & value_cp ) {
      hasValue_m     = value_cp.hasValue_m;  
      id_m           = value_cp.id_m;                
      description_m  = value_cp.description_m;
      value_m        = value_cp.value_m;
      stringIDs_m    = value_cp.stringIDs_m;
      return *this;
   }
}; // class argument_definition {
// ---------------------------------------------------------------------------------------------------------
//typedef std::pair< strings_list, argument_input_definition > args_input_pair;
typedef std::list< argument_definition > args_list;
typedef std::vector< argument_definition > args_vector;
typedef std::pair< std::string, std::size_t > args_pair;
typedef std::multimap< std::size_t, std::size_t > args_id_index_multimap;
typedef std::multimap< std::string, std::size_t > args_string_index_multimap;
// ---------------------------------------------------------------------------------------------------------

template< class map_fabric >
class args {
private:
   args_vector                argsDefinition_m;
   args_id_index_multimap     argsIdIndex_m;
   args_string_index_multimap argsStrIndex_m;
   ids_set                    definedIDs_m;
   strings_set                definedStrings_m;
   strings_list               unknownArgs_m;
   // ------------------------------------------------------------------------------------------------------
   
   void init ( std::size_t argsCount_p, char ** args_p, const args_list & args_list_p ) {
      assert( argsCount_p > 0 && NULL != args_p );
      assert( argsDefinition_m.empty() && argsIdIndex_m.empty() && argsStrIndex_m.empty() 
               && definedIDs_m.empty() && definedStrings_m.empty() && unknownArgs_m.empty() );
      if ( args_list_p.empty() ) return ;
      // �������� ������� ���������� ��� ���������� �������� ��� ������ �� ���������� ��������������
      for ( args_list::const_iterator args_it = args_list_p.begin(); 
            args_list_p.end() != args_it; ++args_it ) {
         for ( strings_list::const_iterator arg_str_it = args_it->stringIDs().begin(); 
               args_it->stringIDs().end() != arg_str_it; ++arg_str_it )
            argsStrIndex_m.insert( std::make_pair( *arg_str_it, argsDefinition_m.size() ) );
         argsIdIndex_m.insert( std::make_pair( args_it->id(), argsDefinition_m.size() ) );
         argsDefinition_m.push_back( *args_it );
      }
      // ������ ���������� ��������� ������
      args_string_index_multimap::const_iterator found_cl = argsStrIndex_m.end();
      for ( std::size_t i = 1; i < argsCount_p; ++i ) {
         if ( argsStrIndex_m.end() == found_cl || !argsDefinition_m.at( found_cl->second ).hasValue() ) {
            const std::string argString_cl = args_p[ i ];
            found_cl = argsStrIndex_m.find( argString_cl );
            if ( argsStrIndex_m.end() == found_cl ) {
               unknownArgs_m.push_back( argString_cl );
               continue;
            }
            args_vector::const_reference foundParametr_cl = argsDefinition_m.at( found_cl->second );
            definedStrings_m.insert(   foundParametr_cl.stringIDs().begin(), 
                                       foundParametr_cl.stringIDs().end() );
            definedIDs_m.insert( foundParametr_cl.id() );
         } else {
            argsDefinition_m[ found_cl->second ].setValue( args_p[ i ] );
            found_cl = argsStrIndex_m.end();
         }
      }  // for ( std::size_t i = 1; i < argsCount_p; ++i ) {
   }  // void init ( ...
   // ------------------------------------------------------------------------------------------------------
public:

   args ( int argsCount_p, char ** args_p, map_fabric & arguments_fabric_p = map_fabric() ) : 
      argsDefinition_m(), argsIdIndex_m(), argsStrIndex_m(), definedIDs_m(), definedStrings_m(), 
      unknownArgs_m()
   {  init( static_cast< std::size_t >( argsCount_p ), args_p, arguments_fabric_p() ); }
   // ------------------------------------------------------------------------------------------------------

   bool isSet ( std::size_t id_p ) const { return definedIDs_m.end() != definedIDs_m.find( id_p ); }
   bool isSet ( const std::string & str_cp ) const 
   {  return definedStrings_m.end() != definedStrings_m.find( str_cp ); } 
   // ------------------------------------------------------------------------------------------------------

   std::string value ( std::size_t id_p ) const {  
      args_id_index_multimap::const_iterator found_l = argsIdIndex_m.find( id_p );
      return argsIdIndex_m.end() == found_l ? std::string() : argsDefinition_m.at( found_l->second ).value(); 
   }
   // ------------------------------------------------------------------------------------------------------

   const strings_list & unknownArgs () const { return unknownArgs_m; }
   // ------------------------------------------------------------------------------------------------------

   std::string makeHelpString () const {
      static const std::string newLine_scl = "\n", minus_scl = " - ";
      std::string result_l;
      for ( args_vector::const_iterator arg_it = argsDefinition_m.begin(); 
            argsDefinition_m.end() != arg_it; ++arg_it ) {
         for ( strings_list::const_iterator str_it = arg_it->stringIDs().begin(); 
               arg_it->stringIDs().end() != str_it; ++str_it ) 
            result_l += newLine_scl + *str_it;
         result_l += minus_scl + arg_it->description();
      }
      result_l += newLine_scl;
      return result_l;
   }
   // ------------------------------------------------------------------------------------------------------
}; // class args {
// ---------------------------------------------------------------------------------------------------------
} // namespace utl {
// ---------------------------------------------------------------------------------------------------------
#endif // UTL_ARGS_H_MRV
// ---------------------------------------------------------------------------------------------------------
