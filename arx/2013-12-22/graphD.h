// ---------------------------------------------------------------------------------------------------------
#ifndef UTL_GRAPH_DATA_TEMPLATES_H_MRV
#define UTL_GRAPH_DATA_TEMPLATES_H_MRV
// ---------------------------------------------------------------------------------------------------------
/*!   \file    graphD.h
      \author  MRV ( mrv_13inbox.ru )
      \date    26.10.2009
      \version 0.2
      \brief   ���������� ������������� ����� � ������ �� ������ ������.
*/
// ---------------------------------------------------------------------------------------------------------
#include <map>
#include <vector>
// ---------------------------------------------------------------------------------------------------------
#include <uShared.h>
#include <uFunctions.h>
// ---------------------------------------------------------------------------------------------------------

/// ������������ ���� ���������� UTL (Useful Trifles Library)
namespace utl {
// ---------------------------------------------------------------------------------------------------------
template <class vertexData_type, class edgeData_type> class edge;
template <class vertexData_type, class edgeData_type> class edgeData;
template <class vertexData_type, class edgeData_type> class vertex;
template <class vertexData_type, class edgeData_type> class vertexData;
// ---------------------------------------------------------------------------------------------------------

template < class vertexData_type, class edgeData_type >
class vertexData
{
//friend template < class VertexDataType_t, class EdgeDataType_t > class tVertex;
//friend class tVertex< class VertexDataType_t, class EdgeDataType_t >;
public:
	vertexData_type data_m;

	std::map< std::size_t, edge< vertexData_type, edgeData_type > > inEdges_m;
	std::map< std::size_t, edge< vertexData_type, edgeData_type > > outEdges_m;
   // ------------------------------------------------------------------------------------------------------

	vertexData () {}
   // ------------------------------------------------------------------------------------------------------

	vertexData ( const vertexData_type & data_cp ) : data_m( data_cp ) {}
   // ------------------------------------------------------------------------------------------------------

	~tVertexData () {

	/*	[��������] ������ ����� �� ������ ������� �����!?!
		���� ������ (utl::shared) ��������� � �������� �����, ������
		���������� ������� �� ����� ������, � ��������� ������ 
		������������� � ��� ��������. 
		// �������� �������� �����
		for ( std::size_t i = 0; i < vOutEdges.size(); ++i )
			vOutEdges[i].end().dellInEdge( vOutEdges[i] );
		vOutEdges.clear();

		// �������� ������� �����
		for ( std::size_t i = 0; i < vInEdges.size(); ++i )
			vInEdges[i].begin().dellOutEdge( vInEdges[i] );
		vInEdges.clear();
	/**/
   }
   // ------------------------------------------------------------------------------------------------------
}; // class vertexData
// ---------------------------------------------------------------------------------------------------------

/*!	\brief ������-����� ������� �����

	-
*/
template < class vertexData_type, class edgeData_type >
class vertex : private shared< vertexData< vertexData_type, edgeData_type > > 
{
public:
	typedef  typename std::map< std::size_t, edge< vertexData_type, edgeData_type > > edges_map;
	typedef  typename std::map< std::size_t, edge< vertexData_type, edgeData_type > >::iterator 
            edges_iterator;
	typedef  typename std::map< std::size_t, edge< vertexData_type, edgeData_type > >::const_iterator 
            edges_const_iterator;
   // ------------------------------------------------------------------------------------------------------
private:
   typedef vertexData< vertexData_type, edgeData_type >  vertexSecuredData;
	typedef shared< vertexSecuredData >                   vertex_sh;
	// ------------------------------------------------------------------------------------------------------
public:

	vertex () : vertex_sh() {}
   // ------------------------------------------------------------------------------------------------------

	vertex ( const vertex< vertexData_type, edgeData_type > & etalon_cp ) : vertex_sh( etalon_cp ) {}
   // ------------------------------------------------------------------------------------------------------

	vertex( const vertexData_type & data_cp ) : vertex_sh( new vertexSecuredData( data_cp ) ) {}
   // ------------------------------------------------------------------------------------------------------

	virtual ~vertex() {}
   // ------------------------------------------------------------------------------------------------------

	bool noData () const { return isNull() || !copyCounter(); }
   // ------------------------------------------------------------------------------------------------------

   const vertexData_type & constData () const { return vertex_sh::constData().data_m; }
   // ------------------------------------------------------------------------------------------------------

   vertexData_type & data () { return vertex_sh::data().data_m; }
   // ------------------------------------------------------------------------------------------------------

	std::size_t inEdgesSize () const { return vertex_sh::constData().inEdges_m.size(); }
   // ------------------------------------------------------------------------------------------------------

	std::size_t outEdgesSize () const { return vertex_sh::constData().outEdges_m.size(); }
   // ------------------------------------------------------------------------------------------------------

	edges_iterator inEdgesBegin () { return vertex_sh::data().inEdges_m.begin(); }
   // ------------------------------------------------------------------------------------------------------

	edges_iterator outEdgesBegin () { return vertex_sh::data().outEdges_m.begin(); }
   // ------------------------------------------------------------------------------------------------------

	edges_const_iterator inEdgesBegin () const { return vertex_sh::constData().inEdges_m.begin(); }
   // ------------------------------------------------------------------------------------------------------

	edges_const_iterator outEdgesBegin () const { return vertex_sh::constData().outEdges_m.begin(); }
   // ------------------------------------------------------------------------------------------------------

	edges_const_iterator inEdgesEnd() const { return vertex_sh::constData().inEdges_m.end(); }
   // ------------------------------------------------------------------------------------------------------

	edges_const_iterator outEdgesEnd() const { return vertex_sh::constData().outEdges_m.end(); }
   // ------------------------------------------------------------------------------------------------------

   std::size_t addInEdge ( edge< vertexData_type, edgeData_type > & edge_p ) {	
		const std::size_t key_cl = 
         smartMapInsert< edge< vertexData_type, edgeData_type> >( edge_p, vertex_sh::data().inEdges_m ); 
		edge_p.setEnd( key_cl, *this );
		return key_cl;
	}
   // ------------------------------------------------------------------------------------------------------

	std::size_t addOutEdge ( edge< vertexData_type, edgeData_type > & edge_p ) {	
		const std::size_t key_cl = 
			smartMapInsert< edge< vertexData_type, edgeData_type > >( edge_p, vertex_sh::data().outEdges_m ); 
		edge_p.setBegin( key_cl, *this );
		return key_cl;
	}
   // ------------------------------------------------------------------------------------------------------

	bool dellInEdge( std::size_t key_p ) {  
      return mapDelete< std::size_t, edge< vertexData_type, edgeData_type > >( 
               key_p, vertex_sh::data().inEdges_m ); 
   }
   // ------------------------------------------------------------------------------------------------------

	bool dellOutEdge( std::size_t key_p ) {	
      return mapDelete< std::size_t, edge< vertexData_type, edgeData_type > >( 
               key_p, vertex_sh::data().outEdges_m ); 
   }
   // ------------------------------------------------------------------------------------------------------

	edges_const_iterator inEdge( std::size_t key_p ) const
	{	return vertex_sh::constData().inEdges_m.find( key_p ); }
   // ------------------------------------------------------------------------------------------------------

	edges_const_iterator outEdge( std::size_t key_p ) const
	{	return vertex_sh::constData().outEdges_m.find( key_p ); }
   // ------------------------------------------------------------------------------------------------------

	bool operator == ( const vertex< vertexData_type, edgeData_type > & etalon_cp ) const 
	{ return vertex_sh::operator ==( etalon_cp ) ? true : etalon_cp.constData() == constData(); }
   // ------------------------------------------------------------------------------------------------------
}; // class vertex
// ---------------------------------------------------------------------------------------------------------

template < class vertexData_type, class edgeData_type >
class edgeData 
{
public:
	edgeData_type	data_m;

	std::size_t beginKey_m, 
					endKey_m;

	vertex< vertexData_type, edgeData_type >  beginVertex_m,	
                                             endVertex_m;
   // ------------------------------------------------------------------------------------------------------

	edgeData () : data_m(), beginKey_m( -1 ), endKey_m( -1 ), beginVertex_m(), endVertex_m() { ; }
   // ------------------------------------------------------------------------------------------------------

	edgeData ( const edgeData_type & data_cp ) : 
		data_m	      ( data_cp ),
		beginKey_m	   ( -1 ),
		endKey_m	      ( -1 ),
		beginVertex_m	( ),
		endVertex_m	   ( )
	{	}
   // ------------------------------------------------------------------------------------------------------
		
	/*	���� ������ ��������� � �������� �����, ������
		���������� ������� �� ����� ������, � ��������� 
		������ ������������� � ��� ��������.
	~edgeData ()
	{
		beginVertex.dellOutEdge( *this );
		endVertex.dellInEdge( *this );
	}	*/

	bool operator == ( const edgeData & etalon_cp ) const 
	{	return	(  	beginVertex_m == etalon_cp.beginVertex_m   && endVertex_m   == etalon_cp.endVertex_m 
				      && beginKey_m    == etalon_cp.beginKey_m      && endKey_m      == etalon_cp.endKey_m ) 
                        ? true : data_m == etalon_cp.data_m; }
   // ------------------------------------------------------------------------------------------------------
}; // class edgeData
// ---------------------------------------------------------------------------------------------------------

/*!	\brief ������-����� ����� �����

	-
*/
template <class vertexData_type, class edgeData_type >
class edge : private shared< edgeData< vertexData_type, edgeData_type > > 
{
friend class vertex< vertexData_type, edgeData_type >;
	//
private:
   typedef edgeData< vertexData_type, edgeData_type > edgeSecuredData;
	typedef shared< edgeSecuredData > edge_sh;
	// ------------------------------------------------------------------------------------------------------

	void setBegin( std::size_t key_p, const vertex< vertexData_type, edgeData_type > & vertex_cp )
	{	
      edge_sh::data().beginKey_m = key_p; 
      edge_sh::data().beginVertex_m = vertex_cp; 
   }
   // ------------------------------------------------------------------------------------------------------

	void setEnd( std::size_t key_p, const vertex< vertexData_type, edgeData_type > & vertex_cp )
	{	
      edge_sh::data().endKey_m = key_p; 
      edge_sh::data().endVertex_m = vertex_cp; 
   }
   // ------------------------------------------------------------------------------------------------------
public:

	edge () : edge_sh() {}
   // ------------------------------------------------------------------------------------------------------

	edge ( const edgeSecuredData & etalon_cp ) : edge_sh( etalon_cp ) {}
   // ------------------------------------------------------------------------------------------------------

	edge ( const edgeData_type & data_cp ) : edge_sh( new edgeSecuredData( data_cp ) ) {}
   // ------------------------------------------------------------------------------------------------------

	virtual ~edge () {}
   // ------------------------------------------------------------------------------------------------------

   edgeData_type & data() { return edge_sh::data().data_m; }
   // ------------------------------------------------------------------------------------------------------

	const edgeData_type & constData () const { return edge_sh::constData().data_m; }
   // ------------------------------------------------------------------------------------------------------

	std::size_t beginKey () const { return edge_sh::constData().beginKey_m; }
   // ------------------------------------------------------------------------------------------------------

	std::size_t endKey () const {	return edge_sh::constData().endKey_m; }
   // ------------------------------------------------------------------------------------------------------
	
	vertex< vertexData_type, edgeData_type > & begin () { return edge_sh::data().beginVertex_m; }
   // ------------------------------------------------------------------------------------------------------

	vertex< vertexData_type, edgeData_type > & end () { return edge_sh::data().endVertex_m; }
   // ------------------------------------------------------------------------------------------------------

	const vertex< vertexData_type, edgeData_type > & constBegin() const 
   {  return edge_sh::constData().beginVertex_m; }
   // ------------------------------------------------------------------------------------------------------

	const vertex< vertexData_type, edgeData_type > & constEnd() const 
   {  return edge_sh::constData().endVertex_m; }
   // ------------------------------------------------------------------------------------------------------

	bool operator == ( edge< vertexData_type, edgeData_type > & etalon_cp ) const
	{
		/*	� ������ ��������� ������������ � ������ � ����� � ������ �� ������ � �����, 
			�.�. ������� edge_sh::data() ���������� ������ ������ 
			edgeData< vertexData_type, edgeData_type >. 	*/
		return edge_sh::operator == ( etalon_cp ) ? true : etalon_cp.constData() == constData();
	}
   // ------------------------------------------------------------------------------------------------------
}; // class edge
// ---------------------------------------------------------------------------------------------------------
} // namespace utl {
// ---------------------------------------------------------------------------------------------------------
#endif // UTL_GRAPH_DATA_TEMPLATES_H_MRV
// ---------------------------------------------------------------------------------------------------------
