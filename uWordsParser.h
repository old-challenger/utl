// ---------------------------------------------------------------------------------------------------------
#ifndef WORDS_PARSER_H_MRV
#define WORDS_PARSER_H_MRV
// ---------------------------------------------------------------------------------------------------------
/*!   \file		   uWordsParser.h
      \author	   Ruslan V. Malstev ( mrv.work.box@yandex.ru )
      \date		   27.10.2009
      \version	   0.2
      \brief	   
      \copyright  GNU Public License
*/
// ---------------------------------------------------------------------------------------------------------
#include <vector>
#include <iostream>
// ---------------------------------------------------------------------------------------------------------
namespace utl {
// ---------------------------------------------------------------------------------------------------------

template < class char_type >
class wordChecker 
{
public:
   class wordArray : public std::vector< char_type > 
   {
   public:
      wordArray () : std::vector< char_type >() { ; }
      template < class string_type >
      wordArray ( const string_type & string_cp )
      {  insert( end(), string_cp.begin(), string_cp.end() ); }
      wordArray & operator << ( const char_type & char_cp ) { push_back( char_cp ); return *this; }
   };
   // ------------------------------------------------------------------------------------------------------
private:
   const wordArray word_cm;
   std::size_t hitsCounter_m;
   // ------------------------------------------------------------------------------------------------------
public:
   wordChecker ( const wordArray & word_p = wordArray() ) : word_cm( word_p ), hitsCounter_m( 0 ) { ; }
   // ------------------------------------------------------------------------------------------------------

   bool isEnd () const { return word_cm.size() == hitsCounter_m; }
   // ------------------------------------------------------------------------------------------------------

   bool check ( char_type char_p ) 
   {
      if ( word_cm.at( hitsCounter_m ) != char_p ) return false;
      ++hitsCounter_m;
      return true;
   }
   // ------------------------------------------------------------------------------------------------------

   std::size_t hitsCounter () const { return hitsCounter_m; }
   // ------------------------------------------------------------------------------------------------------

   void reset () { hitsCounter_m = 0; }
   // ------------------------------------------------------------------------------------------------------
}; // class wordChecker 
// ---------------------------------------------------------------------------------------------------------

template < class string_type >
class defaultStrategy
{
public:
   template < class inserting_iterator >
   void push_back (  string_type & string_p, 
                     const inserting_iterator & begin_p, const inserting_iterator & end_p ) const 
   {  for ( inserting_iterator it = begin_p; end_p != it; ++it ) string_p += *it; }

   //template <>
   //void push_back< string_type > ( string_type & string_p, string_type & other_p ) const 
   //{  string_p += other_p; }
   // ------------------------------------------------------------------------------------------------------

   //template < class size_type >
   //string_type left ( const string_type & string_p, size_type size_p ) const 
   //{  return string_p.substr( 0, size_p ); }
   // ------------------------------------------------------------------------------------------------------

   bool stringIsEmpty ( const string_type & string_p ) const { return !string_p.length(); }
   // ------------------------------------------------------------------------------------------------------
   bool wordEvent ( const string_type & word_p ) { return false; }
   bool lineEvent ( const std::vector< string_type > & line_p ) { return false; }
   // ------------------------------------------------------------------------------------------------------
}; // class defaultStrategy
// ---------------------------------------------------------------------------------------------------------

template <  class char_type, class string_type, 
            class strategy_type = defaultStrategy< string_type > >
class wordsParser 
{
public:
   typedef std::vector< string_type > words_type;
   typedef std::vector< words_type > lines_type;
   // ------------------------------------------------------------------------------------------------------
private:
   typedef std::pair< bool, std::size_t > check_result;
   // ------------------------------------------------------------------------------------------------------
   const typename wordChecker< char_type >::wordArray wordDelimeter_m, lineDelimeter_m;
   // ------------------------------------------------------------------------------------------------------
   strategy_type  strategy_m;
   lines_type     lines_m;
   // ------------------------------------------------------------------------------------------------------

   /*!   \brief Проверка очередного символа на соответсвие последовательности символов разделителя
         \param currentChar_p - проверяемый (очередной) символ
         \param delimeter_p - объект, осуществляющий проверку
         \param vector_p - массив сток или слов 
         \param insertingElement_p - элемент вставляемый в качестве нового слова или строки

            Для символа currentChar_p проверяется совпадение с последовательностью символов разделителя 
         delimeter_p. В случае если совпали все символы разделителя в массив строк или слов добавляется 
         новый элемент insertingElement_p (по умолчанию пустой элемент). Результатом работы функции является 
         пара ( bool hit, std::size_t sizeForErase ), где hit - результат сравнения с последовательностью 
         разделителя, sizeForInsert - число символов разделителя, которые нужно добавить в том случае если 
         совпадение с последовательность разделителя не полное.
   */
   template< class delimeter_type, class element_type >
   check_result checkDelimeter ( const char_type currentChar_p, delimeter_type & delimeter_p, 
                                 std::vector< element_type > & vector_p, 
                                 const element_type & insertingElement_p = element_type() )
   {
      check_result result_l( false, 0 );
      if ( result_l.first = delimeter_p.check( currentChar_p ) )
      {
         if ( !delimeter_p.isEnd() ) return result_l;
         vector_p.push_back( insertingElement_p );
      }
      else result_l.second = delimeter_p.hitsCounter();
      delimeter_p.reset();
      return result_l;
   }
   // ------------------------------------------------------------------------------------------------------
public:

   wordsParser (  const typename wordChecker< char_type >::wordArray & wordDelimeter_p, 
                  const typename wordChecker< char_type >::wordArray & lineDelimeter_p ) :
      wordDelimeter_m( wordDelimeter_p ), lineDelimeter_m( lineDelimeter_p ), strategy_m(), lines_m()
   {  }
   // ------------------------------------------------------------------------------------------------------
   
   const strategy_type & strategy () const { return strategy_m; }
   // ------------------------------------------------------------------------------------------------------

   const lines_type & lines () const { return lines_m; }
   // ------------------------------------------------------------------------------------------------------

   bool parse ( const char_type * input_p, std::size_t size_p )
   {
      assert( wordDelimeter_m.size() && lineDelimeter_m.size() );
      typedef wordChecker< char_type > delimetersChecker;
      delimetersChecker wordDelimeterChecker_l( wordDelimeter_m ),
                        lineDelimeterChecker_l( lineDelimeter_m );
      check_result   wordDelimeterCheckResult_l( false, -1 ), 
                     lineDelimeterCheckResult_l( false, -1 );
      lines_m.clear();
      lines_m.push_back( words_type( 1 ) );
      std::size_t lastWordID_l = 0, lastLineID_l = 0;
      const char_type * const endChar_cl = input_p + size_p;
      for ( const char_type * currentChar_l = input_p; 
            endChar_cl != currentChar_l; ++currentChar_l )   
      {
         wordDelimeterCheckResult_l = 
            checkDelimeter( *currentChar_l, wordDelimeterChecker_l, lines_m.back() );
         lineDelimeterCheckResult_l = 
            checkDelimeter( *currentChar_l, lineDelimeterChecker_l, lines_m, words_type( 1 ) );
         if ( !( wordDelimeterCheckResult_l.first || lineDelimeterCheckResult_l.first ) )
         {
            if ( wordDelimeterCheckResult_l.second > lineDelimeterCheckResult_l.second )
               strategy_m.push_back( lines_m.back().back(), 
                  wordDelimeter_m.begin(), wordDelimeter_m.begin() + wordDelimeterCheckResult_l.second );
            else if ( lineDelimeterCheckResult_l.second )
               strategy_m.push_back( lines_m.back().back(), 
                  lineDelimeter_m.begin(), lineDelimeter_m.begin() + lineDelimeterCheckResult_l.second );
            //strategy_m.push_back( lines_m.back().back(), *currentChar_l );
            lines_m.back().back() += *currentChar_l;
         }
         else
         {
            if ( wordDelimeterCheckResult_l.first && wordDelimeterChecker_l.isEnd() )
            {
               if ( strategy_m.wordEvent( lines_m.back().at( lastWordID_l ) ) ) return false;
               ++lastWordID_l;
            }
            if ( lineDelimeterCheckResult_l.first && lineDelimeterChecker_l.isEnd() )
            {
               if ( strategy_m.lineEvent( lines_m[ lastLineID_l ] ) ) return false;
               lastWordID_l = 0;
               ++lastLineID_l;
            }
         }
      }
      if ( lines_m.size() && lines_m.back().size() && strategy_m.stringIsEmpty( lines_m.back().back() ) )  
         lines_m.back().pop_back();
      if ( lines_m.size() && !lines_m.back().size() ) lines_m.pop_back();
      return true;
   }
   // ------------------------------------------------------------------------------------------------------
}; // class wordsParser 
// ---------------------------------------------------------------------------------------------------------
} // namespace utl
// ---------------------------------------------------------------------------------------------------------
#endif // WORDS_PARSER_H_MRV
// ---------------------------------------------------------------------------------------------------------