// ---------------------------------------------------------------------------------------------------------
#ifndef UTL_PERCENTS_H_MRV
#define UTL_PERCENTS_H_MRV
// ---------------------------------------------------------------------------------------------------------
/*!   \file       uPersents.h
      \author     MRV ( mrv.work.box@yandex.ru )
      \date       2010
      \version    0.1
      \brief      
      \copyright  GNU Public License
*/
// ---------------------------------------------------------------------------------------------------------
namespace utl {
// ---------------------------------------------------------------------------------------------------------

template< class percents_type, class values_type >
class percent
{
private:
   percents_type current_m;
   values_type maxValue_m;
   // ------------------------------------------------------------------------------------------------------
public:

   static percents_type compute ( const values_type & value_p, const values_type & maxValue_p )
   {  return static_cast< percents_type >( value_p * 100 / maxValue_p ); }
   // ------------------------------------------------------------------------------------------------------

   percent () : current_m( 0 ), maxValue_m( 1 ) {}
   // ------------------------------------------------------------------------------------------------------

   percent ( const values_type & maxValue_p ) : current_m( 0 ), maxValue_m( maxValue_p ) 
   { assert( maxValue_p ); }
   // ------------------------------------------------------------------------------------------------------

   const values_type & maxValue () const { return maxValue_m; }
   // ------------------------------------------------------------------------------------------------------

   const percents_type & get () const { return current_m; }
   // ------------------------------------------------------------------------------------------------------

   void setMaxValue ( const values_type & value_p ) { assert( value_p ); maxValue_m = value_p; }
   // ------------------------------------------------------------------------------------------------------

   bool set ( const values_type & value_p ) 
   { 
      assert( value_p <= maxValue_m );
      const percents_type newPercent_l = compute( value_p, maxValue_m );
      if ( current_m != newPercent_l ) 
      {
         current_m = newPercent_l;
         return true;
      }
      return false;
   }
   // ------------------------------------------------------------------------------------------------------
}; // class percent
// ---------------------------------------------------------------------------------------------------------
} // namespace utl
// ---------------------------------------------------------------------------------------------------------
#endif //UTL_PERCENTS_H_MRV
// ---------------------------------------------------------------------------------------------------------