// ---------------------------------------------------------------------------------------------------------	
#ifndef UTL_FUNCTIONS_H_MRV
#define UTL_FUNCTIONS_H_MRV
// ---------------------------------------------------------------------------------------------------------
/*!   \file		   uFunctions.h
      \author	   MRV (mrv.work.box@yandex.ru)
      \date		   9.03.2017
      \version	   1.1.1
      \brief	   Полезные функции
      \copyright  GNU Public License
	
	Файл содержит следующие функции:
	v.0.3
   <ol>
	   <li> [created] abs - модуль числа;
	   <li> [created] sign - знак числа;
	   <li> [created] sign_right - знак числа (ноль положительный);
	   <li> [created] sign_left - знак числа (ноль отрицательный);
	   <li> [created] threeXOR - тристабильный оператор XOR;
	   <li> [created] fourXOR - четырехстабильный оператор XOR.
   </ol>

	v.0.4
	<ol>
      <li> [created] to_within - число c заданной точностью;
	   <li> [created] modulo - расширенное значение по-модулю.
   </ol>

	v.0.5
   <ol>
	   <li> [created] threeXOR - направленный XOR
	   <li> [created] fourXOR - направленный несимметричный XOR
   </ol>

	v.0.6
   <ol>
	   <li> [created] mapInsert - вставка в контейнер map
	   <li> [created] mapDelete - удаление из контейнера map 
   </ol>
	
	v.0.7
   <ol>
	   <li> [created] safeSubtract - вычитание беззнаковых чисел с получением результата со знаком
	   <li> [created] moduloSubtract - вычитание по модулю
   </ol>

	v.0.8
   <ol>
   	<li> [created] justifyString - юстировка строки
   </ol>
	
	v.0.9
   <ol>
      <li> [created] pi - число Пи, константа.
      <li> [created] degrees2radians
   </ol>

   v.1.0.0
   <ol>
      <li> [created] splitString - разделение строки на список строк
   </ol>

   v.1.1.0
   <ol>
      <li> [created] Функция mapInsert переименована в smartMapInsert, алгоритм переписан с ускорением
      <li> [created] mapInsert - добавление элемента в отображение с проверкой 
      <li> [created] mapDelete - выполнено обобщение функции для любых типов ключей
   </ol>

   v.1.1.1
      [created] flatDegrees() - константа 180 градусов
*/
// ---------------------------------------------------------------------------------------------------------
#include <map>
#include <list>
#include <cmath>
#include <string>
// ---------------------------------------------------------------------------------------------------------
namespace utl {
// ---------------------------------------------------------------------------------------------------------

/// @name математические константы
/// @{

/*!	\brief Число Пи
*/
template< class float_type >
inline const float_type pi () { 
   static const float_type value_scl = static_cast< float_type >( 3.1415926535897932384626433832795 );
   return value_scl; 
}

/* \brief Величина плоского угла в градусах
*/
template< class numeric_type >
inline const numeric_type flatDegrees () {
   static const numeric_type value_scl = 180;
   return value_scl;
}
/// @}
// ---------------------------------------------------------------------------------------------------------

/// @name Группа макросов, реализующих простейшие математические функции
/// @{	
// ---------------------------------------------------------------------------------------------------------

/*!	\brief Значение по модулю
*/
template< class numeric_type >
inline const numeric_type abs ( const numeric_type & didgit_cp ) { 
   return didgit_cp < 0 ? -didgit_cp : didgit_cp; 
}
// ---------------------------------------------------------------------------------------------------------

/*!   \brief Перевод градусов в радианы
*/
template< class radians_type, class degrees_type >
inline const radians_type degrees2radians ( const degrees_type & value_p ) {  
   return pi< radians_type >() * value_p / flatDegrees< degrees_type >(); 
}
// ---------------------------------------------------------------------------------------------------------

/*!   \brief Перевод радиан в градусы
*/
template< class degrees_type, class radians_type >
inline const degrees_type radians2degrees ( const radians_type & value_p ) {  
   return static_cast< degrees_type >( value_p / pi< radians_type >() * flatDegrees< degrees_type >() ); 
}
// ---------------------------------------------------------------------------------------------------------

/*!   \brief Целочисленное значение логарифа по основанию, кратному 2
*/
template< class integer_type >
inline integer_type intBinLog ( integer_type operand_p, integer_type step_p ) {  
   integer_type   result_l = 0, 
                  rest_l   = operand_p;
   while( rest_l >>= step_p )
      ++result_l;
   return result_l; 
}
// ---------------------------------------------------------------------------------------------------------

/*!   \brief Число c заданной точностью

      Отбрасываются все знаки после заданного параметром within_cp разряда. Например, для точности в три 
   знака после запятой within_cp = 0.001, для точности в три знака перед запятой within_cp = 1000.
*/
template< class num_type >
inline const num_type to_within ( const num_type & value_cp, const num_type & within_cp ) {	
   return floor( value_cp / within_cp ) * within_cp; 
}
// ---------------------------------------------------------------------------------------------------------

/*!   \brief Расширенное значение по-модулю module_cp
      \return То, что отбрасывается функцией utl::to_within.

      Параметр module_cp передается в параметр within_cp функции utl::to_within().
*/
template< class num_type >
inline const num_type modulo ( const num_type & value_cp, const num_type & module_cp ) {
   num_type const abs_cl = abs( value_cp );
   return ( abs_cl - to_within( abs_cl, module_cp ) ) * sign( value_cp );
}
// ---------------------------------------------------------------------------------------------------------

/*!	\brief Знак выражения 
*/
template< class num_type >
inline const num_type sign ( const num_type & didgit_cp ) {	
   return didgit_cp > 0 ? num_type( 1 ) : didgit_cp < 0 ? num_type( -1 ) : 0; 
}
// ---------------------------------------------------------------------------------------------------------

/*!	\brief Знак выражения, ноль положительный
*/
template< class num_type >
inline const num_type sign_right ( const num_type & didgit_cp ) {	
   return didgit_cp < 0 ? static_cast< num_type >( -1 ) : static_cast< num_type >( 1 ); 
}
// ---------------------------------------------------------------------------------------------------------

/*!	\brief Знак выражения, ноль отрицательный
*/
template< class num_type >
inline const num_type sign_left ( const num_type & didgit_cp ) { 
   return didgit_cp > 0 ? static_cast< num_type >( 1 ) : static_cast< num_type >( -1 ); 
}
// ---------------------------------------------------------------------------------------------------------

/*!   \brief "Безопасное" вычитание беззнаковых чисел
      \return Знаковый результат вычитания беззнаковых чисел
*/
template< class num_t, class unsigned_num_t >
inline const num_t safeSubtract (	const unsigned_num_t & minuend_p, 
         									const unsigned_num_t & subtrahend_p	) {
	return minuend_p < subtrahend_p 
            ? - static_cast< num_t >( subtrahend_p - minuend_p ) 
            : + static_cast< num_t >( minuend_p - subtrahend_p );
}
// ---------------------------------------------------------------------------------------------------------

/*! \brief Вычитание по модулю modulo_p
*/
template< class num_t, class unsigned_num_t >
inline const num_t moduloSubtract ( const unsigned_num_t & minuend_p, 
                                    const unsigned_num_t & subtrahend_p, 
                                    const num_t & modulo_p ) {
	num_t const sub_cl = safeSubtract< num_t, unsigned_num_t >( minuend_p, subtrahend_p );
	return ( sub_cl < 0 ? modulo_p : 0 ) + sub_cl;
}
// ---------------------------------------------------------------------------------------------------------
/// @}
// ---------------------------------------------------------------------------------------------------------

/// @name Группа Функций для логических данных
/// @{	

/*!   \brief Направленный (тристабильный) XOR
      \param left_p - левый параметр
      \param right_p - правый параметр
      \return см. таблицу

   <ol>
      <li><t/>left<t/>right<t/>return
      <li><t/> 0 <t/> 0 <t/> 0
      <li><t/> 1 <t/> 0 <t/> -1
      <li><t/> 0 <t/> 1 <t/> 1
      <li><t/> 1 <t/> 1 <t/> 0
   </ol>
*/
template< class int_type >  
inline int_type threeXOR ( bool left_p, bool right_p ) {  
   return static_cast< int_type >( !left_p && right_p ? 1 : left_p && !right_p ? -1 : 0 ); 
}
// ---------------------------------------------------------------------------------------------------------

/*!   \brief Направленный (тристабильный) XOR
      \param left_p - левый параметр
      \param right_p - правый параметр
      \return см. таблицу

      <ol>
         <li><t/>left<t/>right<t/>return
         <li><t/> 0 <t/> 0 <t/> 0
         <li><t/> 1 <t/> 0 <t/> -1
         <li><t/> 0 <t/> 1 <t/> 1
         <li><t/> 1 <t/> 1 <t/> 2
      </ol>
*/
template< class int_type >  
inline int_type fourXOR ( bool left_p, bool right_p ) {	
   return static_cast< int_type >( !left_p & right_p 
      ? 1 : left_p & !right_p ? -1 : left_p & right_p ? 2 : 0 ); 
}
// ---------------------------------------------------------------------------------------------------------
/// @}
// ---------------------------------------------------------------------------------------------------------

/// @name Группа Функций для работы с контейнерами библиотеки STL
/// @{	

/*!   \brief Вставка элемента element_cp в отображение map_p
      \param key_cp - вставляемое ключевое значение
      \param data_cp - вставляемый элемнт данных
      \param map_rp - отображение в которое производится вставка
      \return Идентификационный номер вставленного элемента
*/
template< class key_type, class data_type >
inline typename std::map< key_type, data_type >::iterator 
mapInsert( const key_type & key_cp, const data_type & data_cp, std::map< key_type, data_type > & map_rp ) {
   const std::pair< key_type, data_type > element_cl( key_cp, data_cp );
   typename std::map< key_type, data_type >::const_iterator found_l = map_rp.lower_bound( key_cp );
   if ( map_rp.end() == found_l || key_cp < found_l->first ) 
      return map_rp.insert( found_l, element_cl );
   return map_rp.end();
}
// ---------------------------------------------------------------------------------------------------------

/*!   \brief Вставка нового или замена существующего элемента element_cp  в отображении map_p 
      \param key_cp - вставляемое ключевое значение
      \param data_cp - вставляемый элемнт данных
      \param map_rp - отображение в которое производится вставка
*/
template< class key_type, class data_type >
inline void //typename std::map< key_type, data_type >::iterator 
mapInsertOrReplace(  const key_type & key_cp, const data_type & data_cp, 
                     std::map< key_type, data_type > & map_rp ) {
   const std::pair< key_type, data_type > element_cl( key_cp, data_cp );
   typename std::map< key_type, data_type >::iterator found_l = map_rp.lower_bound( key_cp );
   if ( map_rp.end() == found_l || key_cp < found_l->first ) 
      map_rp.insert( found_l, element_cl );
   else 
      found_l->second = element_cl.second;
}
// ---------------------------------------------------------------------------------------------------------

/*!   \brief Вставка пвары значений ( key_cp, data_cp ) в отображение map_p
      \param data_cp - вставляемый элемнт данных
      \param map_rp - отображение в которое производится вставка
      \return Идентификатор добавленного элемента

      Вставляемому эелементу присваивается самый маленький свободный идентификационный номер.
*/
template< class data_type >
inline std::size_t smartMapInsert ( const data_type & data_cp, 
                                    std::map< std::size_t, data_type > & map_rp ) {
	std::size_t currentKey_l = map_rp.size();
   typename std::map< std::size_t, data_type >::const_iterator found_l;
   while( 1 ) {
      found_l = map_rp.lower_bound( currentKey_l );
      if ( map_rp.end() == found_l || found_l->first != currentKey_l ) 
         break;
      --currentKey_l;
   }
   map_rp.insert( found_l, std::make_pair( currentKey_l, data_cp ) );
	return currentKey_l;
}
// ---------------------------------------------------------------------------------------------------------

/*!   \brief Удаление элемента с идентификационным номером key_p из отображения map_p
      \param key_p - идентификатор удаляемого элемента 
      \param map_p - отображение из которого производится удаление
      \return Результат удаления
*/
template< class key_type, class data_type >
inline bool mapDelete ( const key_type & key_p, std::map< key_type, data_type > & map_rp ) {
	const typename std::map< key_type, data_type >::const_iterator found_cl = map_rp.find( key_p );
	const bool result_l = map_rp.end() != found_cl;
	if ( result_l ) 
      map_rp.erase( found_cl );
   return result_l;
}
// ---------------------------------------------------------------------------------------------------------

/*!   \brief Вставка в начало строки элементов filler_p до размера newLength_p
      \param str_p - исходная строка
      \param newLength_p - длина формируемой строки
      \param filler_p - заполнитель
      \return Идентификационный номер вставленного элемента
*/
template <  class char_type, class string_type >
inline string_type justifyString ( const string_type & str_p, std::size_t newLength_p, char_type filler_p ) {
	if ( str_p.length() >= newLength_p ) 
      return str_p;
	string_type result_l;
	result_l.insert( result_l.begin(), newLength_p - str_p.length(), filler_p );
	result_l += str_p;
	return result_l;
}
// ---------------------------------------------------------------------------------------------------------

/*!   \brief Вставка в начало std-строки элементов filler_p до размера newLength_p
      \param str_p - исходная std-строка
      \param newLength_p - длина формируемой std-строки
      \param filler_p - заполнитель
      \return Идентификационный номер вставленного элемента
*/
template< class char_type >
inline 
std::basic_string< char_type, std::char_traits< char_type >, std::allocator< char_type > > 
justifySTLString (   const std::basic_string<   char_type, 
                                                std::char_traits< char_type >, 
                                                std::allocator< char_type > >   & str_p, 
                     std::size_t newLength_p, char_type filler_p ) {
   return justifyString<   char_type, 
                           std::basic_string< char_type, std::char_traits< char_type >, 
                              std::allocator< char_type > > >( str_p, newLength_p, filler_p );
}
// ---------------------------------------------------------------------------------------------------------

/*!   \brief Разделение строки на список строк
      \param string_cp - исходная строка
      \param delimeter_cp - символ
      \return Список строк, разделённых разделителем delimeter_cp в исходной строке
*/
template < class char_type, class string_type >
std::list< string_type > splitString( const string_type & string_cp, const char_type & delimeter_cp ) {
   string_type buff_l;
   std::list< string_type > result_l;
   for ( typename string_type::const_iterator it = string_cp.begin(); string_cp.end() != it; ++it )
      if ( delimeter_cp == *it ) {
         result_l.push_back( buff_l );
         buff_l.clear();
      }
      else buff_l += *it;
   if ( buff_l.length() ) 
      result_l.push_back( buff_l );
   return result_l;
}
// ---------------------------------------------------------------------------------------------------------

/*!   \brief Разделение stl-строки на список строк
      \param string_cp - исходная stl-строка
      \param delimeter_cp - символ
      \return Список stl-строк, разделённых разделителем delimeter_cp в исходной stl-строке
*/
template < class char_type >
inline
std::list< std::basic_string< char_type, std::char_traits< char_type >, std::allocator< char_type > > >
splitSTLString(   const std::basic_string<   char_type, 
                                             std::char_traits< char_type >, 
                                             std::allocator< char_type > > & string_cp, 
                  const char_type & delimeter_cp ) {
   return splitString< char_type, 
      std::basic_string< char_type, std::char_traits< char_type >, std::allocator< char_type > > 
         >( string_cp, delimeter_cp );
}
// ---------------------------------------------------------------------------------------------------------

//template < class char_type, class string_type >
//std::vector< char_type > strToVector ( const string_type & string_cp )
//{
//   std::vector< char_type > result_l;
//   result_l.reserve( string_cp.length() )
//   for ( std::size_t i = 0; i < string_cp.length(); ++i ) result_l.push_back( string_cp.at( i ) );
//   return result_l
//}
// ---------------------------------------------------------------------------------------------------------
/// @}
// ---------------------------------------------------------------------------------------------------------

/// @name Группа функций для работы с алгоритмами библиотеки STL
/// @{	
// ---------------------------------------------------------------------------------------------------------

template < class variable_type, variable_type const_value >
inline void set ( variable_type & variable_p ) { variable_p = const_value; }
// ---------------------------------------------------------------------------------------------------------
/// @}
// ---------------------------------------------------------------------------------------------------------

/// @name Группа функций, осуществляющих побитовые операции с данными
/// @{	
// ---------------------------------------------------------------------------------------------------------

template < class result_type, class byte_type >
result_type reverse ( const byte_type * in_cp, std::size_t size_p ) { 
   static const result_type step_scl = sizeof( byte_type ) * 8;
   result_type result_l = 0;
   for ( std::size_t i = 0; i < size_p; ++i )  
      result_l = ( result_l << step_scl ) + in_cp[ i ]; 
   return result_l;
}
// ---------------------------------------------------------------------------------------------------------
/// @}
// ---------------------------------------------------------------------------------------------------------

/*	Добавить:
	1.	log-макросы. а может сделать отдельный класс ??
			#define OPEN_LOG (constFileName)
			аналогично отладке, но еще можно написать класс.
	2.	Подумать про исключения.
*/
// ---------------------------------------------------------------------------------------------------------
} // namespace utl {
// ---------------------------------------------------------------------------------------------------------
#endif // UTL_FUNCTIONS_H_MRV
// ---------------------------------------------------------------------------------------------------------
