// ---------------------------------------------------------------------------------------------------------	
#ifndef UTL_ARGS_H_MRV
#define UTL_ARGS_H_MRV
// ---------------------------------------------------------------------------------------------------------
/*!   \file       uArgs.h
      \author     MRV ( mrv.work.box@yandex.ru )
      \date       2013
      \version    0.2
      \brief   
      \copyright  GNU Public License
*/
// ---------------------------------------------------------------------------------------------------------
#include <map>
#include <set>
#include <string>
#include <vector>
#include <cassert>
// ---------------------------------------------------------------------------------------------------------
#include <uTypes.h>
#include <uShared.h>
// ---------------------------------------------------------------------------------------------------------
namespace utl {
// ---------------------------------------------------------------------------------------------------------

class argument_data {
public:
   argument_data ( std::size_t id_p, const strings_list & keyStrings_cp, const std::string & description_cp, bool 
                  hasValue_p = false, const std::string & defaultValue_cp = std::string() ) : 
      hasValue_cm( hasValue_p ), description_cm( description_cp ), defaultValue_cm( defaultValue_cp ), 
      id_cm( id_p ), keyStrings_cm( keyStrings_cp ), values_m() { 
      if ( !defaultValue_cp.empty() ) values_m.push_back( defaultValue_cp ); 
   }

   argument_data ( const argument_data & etalon_cp ) : 
      hasValue_cm( etalon_cp.hasValue_cm ), description_cm( etalon_cp.description_cm ), 
      defaultValue_cm( etalon_cp.defaultValue_cm ), id_cm( etalon_cp.id_cm ), 
      keyStrings_cm( etalon_cp.keyStrings_cm ), values_m( etalon_cp.values_m )
   { ; }
   //
   bool hasValue () const { return hasValue_cm; }

   const std::string &  description    () const { return description_cm; }
   std::size_t          id             () const { return id_cm; }
   const strings_list & keyStrings     () const { return keyStrings_cm; }
   const strings_list & values         () const { return values_m; }
   
   void addValue ( const std::string & value_cp ) {
      // Если в списке значение присутствует только значение по-умолчанию - оно заменяется на добавляемое
      if ( 1 == values_m.size() && defaultValue_cm == values_m.front() )
         values_m.front() = value_cp;
      else 
         values_m.push_back( value_cp );
   }
   

   //const argument_definition & operator = ( const argument_definition & value_cp ) {
   //   hasValue_m     = value_cp.hasValue_m;  
   //   description_m  = value_cp.description_m;
   //   defaultValue_m = value_cp.value_m;
   //   stringIDs_m    = value_cp.stringIDs_m;
   //   return *this;
   //}
private:
   bool                 hasValue_cm;      ///< Имеет ли параметр значение или является ключом
   const std::string    defaultValue_cm,  ///< Значение по-умолчанию
                        description_cm;   ///< Описание для формирования текста подсказки
   const std::size_t    id_cm;            ///< Идентификатор для быстррой проверки заданного параметра
   const strings_list   keyStrings_cm;    ///< Текстовые ключи для доступа к аргументу
   strings_list         values_m;         ///< Список заданных значений   
}; // class argument_data {
// ---------------------------------------------------------------------------------------------------------

typedef std::vector< argument_data > args_vector;
// ---------------------------------------------------------------------------------------------------------

class shared_args : private shared< args_vector > {
public:
   shared_args ( std::size_t reserveSize_p ) : shared< args_vector >( true ) { 
      data().reserve( reserveSize_p ); 
   }

   shared_args ( const shared_args & etalon_cp ) : shared< args_vector >( *this ) { ; }

   std::size_t addKeyArgument ( const strings_list & keyStrings_cp, const std::string & description_cp ) {
      data().push_back( argument_data( constData().size(), keyStrings_cp, description_cp ) );
   }

   std::size_t addDataArgument ( const strings_list & keyStrings_cp, const std::string & description_cp,
                                 const std::string & defaultValue_cp ) {
      data().push_back( argument_data( constData().size(), keyStrings_cp, description_cp, true, 
         defaultValue_cp ) );
   }

   const args_vector & list () const { return constData(); }

   const shared_args & operator = ( const shared_args & etalon_cp ) {
      shared< args_vector >::operator =( etalon_cp );
      return *this;
   }
}; // class shared_args

template< class map_fabric >
class args {
public:

   args ( int argsCount_p, char ** args_p, map_fabric & arguments_fabric_p = map_fabric() ) : 
      argsData_m(), argsIdIndex_m(), argsStrIndex_m(), definedIDs_m(), definedStrings_m(), unkeyedArgs_m() {  
      init( static_cast< std::size_t >( argsCount_p ), args_p, arguments_fabric_p() ); 
   }
   // ------------------------------------------------------------------------------------------------------

   bool isSet ( std::size_t id_p ) const { return definedIDs_m.end() != definedIDs_m.find( id_p ); }
   // ------------------------------------------------------------------------------------------------------

   bool isSet ( const std::string & str_cp ) const { 
      return definedStrings_m.end() != definedStrings_m.find( str_cp ); 
   } 
   // ------------------------------------------------------------------------------------------------------

   const strings_list & values ( std::size_t id_p ) const {  
      static const strings_list empyList_scl;
      args_id_index_map::const_iterator found_l = argsIdIndex_m.find( id_p );
      return argsIdIndex_m.end() == found_l ? empyList_scl : argsData_m.at( found_l->second ).values(); 
   }
   // ------------------------------------------------------------------------------------------------------

   const strings_list & unkeyedArgs () const { return unkeyedArgs_m; }
   // ------------------------------------------------------------------------------------------------------

   std::string makeHelpString () const {
      static const std::string newLine_scl = "\n", minus_scl = " - ";
      std::string result_l;
      for ( auto argument_l : argsData_m.list() ) {
         for ( keyString_l : argument_l.keyStrings() ) 
            result_l += newLine_scl + keyString_l;
         result_l += minus_scl + argument_l.description();
      }
      result_l += newLine_scl;
      return result_l;
   }
   // ------------------------------------------------------------------------------------------------------
private:
   typedef std::map< std::size_t, std::size_t > args_id_index_map;
   typedef std::multimap< std::string, std::size_t > args_string_index_multimap;

   shared_args                argsData_m;       // Данные по всем определенным аргументам 
   args_id_index_map          argsIdIndex_m;    // Индекс для, аргиментов имеющих значение
   args_string_index_multimap argsStrIndex_m;   //
   ids_set                    definedIDs_m;     //
   strings_set                definedStrings_m; //
   strings_list               unkeyedArgs_m;    //
   // ------------------------------------------------------------------------------------------------------

   void init ( std::size_t argsCount_p, char ** args_p, const args_shared_vector & argsDefinitionList_cp ) {
      assert( argsCount_p > 0 && nullptr != args_p );
      assert( argsData_m.list().empty() && argsIdIndex_m.empty() && argsStrIndex_m.empty() 
         && definedIDs_m.empty() && definedStrings_m.empty() && unknownArgs_m.empty() );
      if ( argsDefinitionList_cp.constData().empty() ) return ;
      // Создание индексов для поиска аргументов по текстовым и числовому идентификаторам
      argsData_m = argsDefinitionList_cp;
      for ( std::size_t i = 0; argsData_m.list().size() > i; ++i ) {
         for ( auto keyStr : argsData_m.list().at( i ).keyStrings() )
            argsStrIndex_m.insert( std::make_pair( keyStr, i ) );
         argsIdIndex_m.insert( std::make_pair( argsData_m.list().at( i ).id(), i ) );
      }
      // Разбор параметров командной строки
      args_string_index_multimap::const_iterator found_cl = argsStrIndex_m.end();
      for ( std::size_t i = 1; i < argsCount_p; ++i ) {
         if ( argsStrIndex_m.end() == found_cl || !argsData_m.list().at( found_cl->second ).hasValue() ) {
            const std::string argString_cl = args_p[ i ];
            found_cl = argsStrIndex_m.find( argString_cl );
            if ( argsStrIndex_m.end() == found_cl ) {
               unkeyedArgs_m.push_back( argString_cl );
               continue;
            }
            args_vector::const_reference foundParametr_cl = argsData_m.list().at( found_cl->second );
            definedStrings_m.insert( foundParametr_cl.keyStrings().begin(), 
               foundParametr_cl.keyStrings().end() );
            definedIDs_m.insert( foundParametr_cl.id() );
         } else {
            argsData_m.list()[ found_cl->second ].addValue( argString_cl );
            found_cl = argsStrIndex_m.end();
         }
      }  // for ( std::size_t i = 1; i < argsCount_p; ++i ) {
   }  // void init ( ...
   // ------------------------------------------------------------------------------------------------------
}; // class args {
// ---------------------------------------------------------------------------------------------------------
} // namespace utl {
// ---------------------------------------------------------------------------------------------------------
#endif // UTL_ARGS_H_MRV
// ---------------------------------------------------------------------------------------------------------
