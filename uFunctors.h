// ---------------------------------------------------------------------------------------------------------
#ifndef UTL_FUNCTORS_H_MRV
#define UTL_FUNCTORS_H_MRV
// ---------------------------------------------------------------------------------------------------------
/*!
\file		   uFunctors.h
\author	   MRV ( mrv.work.box@yandex.ru )
\date		   20.09.2010
\version	   0.1
\brief	   Полезные функторы.
\copyright  GNU Public License

	Файл содержит следующие функции:
	v.0.1
   <ol>
	   <li> [created] simple - простое преобразование типов static_cast;
	   <li> [created] str2num - преобразование строки в число с учётом системы счисления;
   </ol>

	v.0.4
	<ol>
      <li> [created] 
      <li> [created] 
   </ol>

*/
// ---------------------------------------------------------------------------------------------------------
#include <uTypes.h>

#include <string>
#include <algorithm>
#include <cassert>
// ---------------------------------------------------------------------------------------------------------
namespace utl {
// ---------------------------------------------------------------------------------------------------------

class convertors
{
public:
   convertors () = delete;

   template < class out_t >
   class empty_base {
   public:
      
      empty_base () { ; }
      
      template < class in_t >
      out_t operator () ( const in_t & in_p ) const;
   };
   // ------------------------------------------------------------------------------------------------------

   template < class out_t >
   class outref_base {
   public:
      outref_base () = delete;

      outref_base ( out_t & out_rp ) : out_rm ( out_rp ) { ; }

      template < class in_t >
      const out_t & operator () ( const in_t & in_p ) const;
   protected:
      out_t & out_rm;
   };
   // ------------------------------------------------------------------------------------------------------

   template < class out_t >
   class simple : public empty_base< out_t > {
   public:
      template < class in_t >
      out_t operator () ( const in_t & in_p ) const { return static_cast< out_t >( in_p ); }

      template <> out_t operator ()< out_t > ( const out_t & in_cp ) const { return in_cp; }
   };
   // ------------------------------------------------------------------------------------------------------

   template < class out_t >
   class symbols2num : public empty_base< out_t > {
   public:

      template < class symbol_type > 
      out_t operator () ( symbol_type in_p ) const {
         return isdigit( in_p ) ? static_cast< out_t >( in_p - consts< symbol_type >::zero() ) 
            : isalpha( in_p )
               ? consts< symbol_type >::ten() + ( islower( in_p ) ? in_p - consts< symbol_type >::a()
               : in_p - consts< symbol_type >::A() ) : static_cast< symbol_type >( 0 );
      }
   };
   // ------------------------------------------------------------------------------------------------------

   template < class out_t >
   class str2num : public empty_base< out_t > {
   public:

      template < class string_t > 
      out_t operator () ( const string_t & in_cp, out_t range_p = consts< out_t >::ten() ) const {
         out_t result_l = static_cast< out_t >( 0 );
         std::for_each( in_cp.cbegin(), in_cp.cend(), 
            resultReservoir< type2id< out_t >::VALUE & ( 
               types::TYPES_IDS_SIGNED_INTEGER | types::TYPES_IDS_FLOAT_DOT ) >( result_l, range_p ) );
         return result_l;
      }
   private:
      class baseSymbolsReservoir {
      public:
         baseSymbolsReservoir () = delete;

         baseSymbolsReservoir ( out_t & range_rp, const out_t & range_cp ) : 
            state_m( STATE_NO_ERROR ), range_cm( range_cp ), result_rm( range_rp ) { ; }
      protected:
         enum : char {
            STATE_START    = 0x0,
            STATE_NO_ERROR = 0x1,
            STATE_MINUS    = 0x2,
            STATE_DOT      = 0x4,
            STATE_ERROR    = 0x8
         };

         char state_m;
         const out_t range_cm;
         out_t & result_rm;
      };

      template < id16_t outType_id > class resultReservoir;

      template <>
      class resultReservoir< types::TYPES_IDS_INTEGER > : public baseSymbolsReservoir {
      public:

         resultReservoir ( out_t & range_rp, const out_t & range_cp ) : baseSymbolsReservoir( range_rp, range_cp ) { ; }

         template < class char_type > 
         const out_t & operator () ( char_type symbol_p ) { 
            if ( STATE_NO_ERROR & state_m && isdigit( symbol_p ) ) {
               result_rm *= range_cm;
               result_rm += symbols2num< out_t >()( symbol_p );
            } else {
               state_m = STATE_ERROR;
               result_rm = 0;
            }
            return result_rm;
         }
      };

      template <>
      class resultReservoir< types::TYPES_IDS_SIGNED_INTEGER > : public baseSymbolsReservoir {
      public:

         resultReservoir ( out_t & range_rp, const out_t & range_cp ) : 
            baseSymbolsReservoir( range_rp, range_cp ) { state_m = STATE_START; }

         template < class char_type >
         const out_t & operator () ( char_type symbol_p ) {
            switch ( state_m ) {
               case STATE_START: if ( consts< char_type >::minus() == symbol_p ) {
                     state_m = STATE_MINUS;
                     return result_rm;
                  } else 
                     state_m = STATE_NO_ERROR;
                  break;
               case STATE_MINUS:
                  state_m = STATE_NO_ERROR;
                  return result_rm = - ( ( resultReservoir< types::TYPES_IDS_INTEGER > * ) this )->operator ()( symbol_p );
            }
            return ( ( resultReservoir< types::TYPES_IDS_INTEGER > * ) this )->operator ()( symbol_p );
         }
      };

      template <>
      class resultReservoir< types::TYPES_IDS_FLOAT_DOT > : public baseSymbolsReservoir {
      public:
         resultReservoir ( out_t & range_rp, const out_t & range_cp ) : 
            baseSymbolsReservoir( range_rp, range_cp ), currentExponent_m( 1 ) { state_m = STATE_START; }

         template < class char_type >
         const out_t & operator () ( char_type symbol_p ) { 
            switch ( state_m ) { 
               case STATE_DOT: if ( isdigit( symbol_p ) ) {
                     currentExponent_m /= range_cm;
                     result_rm += currentExponent_m * symbols2num< out_t >()( symbol_p ); 
                  } else { 
                     state_m = STATE_ERROR;
                     result_rm = 0;
                  } 
                  return result_rm;
               case STATE_START:
               case STATE_NO_ERROR: if ( consts< char_type >::dot() == symbol_p ) state_m = STATE_DOT;
               default:
                  return ( ( resultReservoir< types::TYPES_IDS_SIGNED_INTEGER > * ) this )->operator ()( symbol_p );
            }
         }
      private:
         out_t currentExponent_m;
      };
   }; // template class str2num {
   
   template <>
   class str2num< bool > : public empty_base < bool > {
   public:

      template < class string_type > 
      bool operator () ( const string_type & in_cp, bool range_p = true ) const {
         return consts< string_type >::trueTxt() == in_cp ? true : false;
      }
   };
   // ------------------------------------------------------------------------------------------------------

   template < class symbol_type, template< class > class base_template = empty_base >
   class num2symbol : public base_template< symbol_type > {
   public:
      template< class in_type >
      symbol_type operator () ( in_type digit_p ) const {
         assert( digit_p < 37 ); // цифры + английские буквы
         const auto digit_cl = static_cast< symbol_type >( digit_p );
         return consts< symbol_type >::ten() > digit_cl
            ? consts< symbol_type >::zero() + digit_cl
            : consts< symbol_type >::a() + digit_cl - consts< symbol_type >::ten();
      }
   };
   // ------------------------------------------------------------------------------------------------------
   
   template < class out_t, template< class > class base_template = outref_base >
   class num2str : public base_template< out_t > {
   public:

      template < class in_t >
      const out_t & operator () ( in_t in_p, in_t range_p = consts< in_t >::ten() ) const;
   }; 
   // ------------------------------------------------------------------------------------------------------

   template < class out_t >
   class num2str< out_t, outref_base > : public outref_base< out_t > {
   public:

      num2str( out_t & out_rp ) : outref_base< out_t > ( out_rp ) { ; }

      template < class in_t >
      const out_t & operator () ( in_t in_p, in_t range_p = consts< in_t >::ten() ) const {
         static_assert( type2id< in_t >::VALUE & types::TYPES_IDS_SIMPLE,
                        "Incompatible parameter type for num2str::operator () method" );
         static_assert( type2id< out_t >::VALUE & types::TYPES_IDS_STRINGS,
                        "Incompatible return value type for num2str::operator () method" );
         assert ( range_p < 37 ); // ограничение на число символов латиницы: 10 цифр + 27 букв
         if ( out_rm.length() )
            out_rm.clear();
         stringFormer< type2id< in_t >::VALUE & ( types::TYPES_IDS_SIGNED_INTEGER | types::TYPES_IDS_FLOAT_DOT ) >()(
            in_p, range_p, out_rm );
         return out_rm;
      }

      template <>
      const out_t & operator ()< bool > ( bool in_p, bool range_p ) const { 
         return out_rm = in_p ? consts< out_t >::trueTxt() : consts< out_t >::falseTxt();
      }
   private:
      template < id16_t inTypeId_const >
      class stringFormer {
      public:
         template < class in_t >
         const out_t & operator () ( in_t in_p, in_t range_p, out_t & out_rp ) const; 
      };
      
      template <>
      class stringFormer< types::TYPES_IDS_INTEGER > {
      public:
         template < class in_t >
         const out_t & operator () ( in_t in_p, in_t range_p, out_t & out_rp ) const { 
            in_t tail_l = in_p;
            while ( tail_l ) {
               out_rp = num2symbol< typename out_t::value_type >()( tail_l % range_p ) + out_rp;
               tail_l /= range_p;
            }
            return out_rp;
         }
      };

      template <>
      class stringFormer< types::TYPES_IDS_SIGNED_INTEGER > {
      public:
         template < class in_t >
         const out_t & operator () ( in_t in_p, in_t range_p, out_t & out_rp ) const { 
            bool negative_l = 0 > in_p;
            in_t tail_l = negative_l ? -1 * in_p : in_p;
            stringFormer< types::TYPES_IDS_INTEGER >()( tail_l, range_p, out_rp );
            if ( negative_l )
               out_rp = consts< typename out_t::value_type >::minus() + out_rp;
            return out_rp;
         }
      };

      template <>
      class stringFormer< types::TYPES_IDS_FLOAT_DOT > {
      public:
         template < class in_t >
         const out_t & operator () ( in_t in_p, in_t range_p, out_t & out_rp ) const { 
            return out_rp;
         }
      };
   }; // template < class out_t > class num2str< out_t, outref_base > {
   // ------------------------------------------------------------------------------------------------------
   
   template < class out_t >
   class num2str< out_t, empty_base > : public empty_base < out_t > {
   public:

      template < class in_t >
      out_t operator () ( in_t in_p, in_t range_p = consts< in_t >::ten() ) const {
         out_t result_l;
         return num2str< out_t, outref_base > ( result_l )( in_p, range_p );
      }

	  template < class in_t >
     const out_t & operator () ( in_t in_p, out_t & out_rp, in_t range_p = consts< in_t >::ten ( ) ) const {
		  return num2str< out_t, outref_base >( out_rp )( in_p, range_p );
	  }
   }; // template < class out_t > class num2str< out_t, empty_base > {
   // ------------------------------------------------------------------------------------------------------

   template < class out_t, template< class > class base_template = outref_base  >
   class str2str : public outref_base< out_t > { 
   public:
      template < class in_t >
      out_t operator () ( const in_t & in_cp ) const;
   };
   // ------------------------------------------------------------------------------------------------------

   template < class out_t >
   class str2str< out_t, outref_base > : public outref_base< out_t > {
   public:
      
      str2str ( out_t & out_rp ) : outref_base< out_t > ( out_rp ) { ; }

      template < class in_t >
      out_t operator () ( const in_t & in_cp ) const {
         out_rm.resize( in_cp.size() );
         typename out_t::size_type i = 0;
         for ( auto nextSymbol : in_cp )
            out_rm[ i++ ] = simple< typename out_t::value_type >()( nextSymbol );
         return out_rm;
      }

      template <> out_t operator () ( const out_t & in_cp ) const { return in_cp; }
   };
   // ------------------------------------------------------------------------------------------------------

   template < class out_t >
   class str2str< out_t, empty_base > : public empty_base< out_t > {
   public:

      template < class in_t >
      out_t operator () ( const in_t & in_cp ) const {
         out_t result_l;
         return str2str< out_t, outref_base >( result_l )( in_cp );
      }

      template < class in_t >
      out_t operator () ( const in_t & in_cp, out_t & out_rp ) const {
         return str2str< out_t, outref_base > ( out_rp )( in_cp );
      }
   };
   // ------------------------------------------------------------------------------------------------------
private:

   template< class const_type >
   class consts {
   public:
      //const_type zero ( ) const;
      //const_type a ( ) const;
      //const_type A ( ) const;
      //const_type minus ( ) const;
      static const_type ten () { 
         static const const_type value_scl = static_cast< const_type >( 10 ); 
         return value_scl; 
      }
   };
   // ------------------------------------------------------------------------------------------------------

   template<>
   class consts< char > {
   public:
      static char a      () { static const char value_scl = 'a'; return value_scl; }
      static char A      () { static const char value_scl = 'A'; return value_scl; }
      static char dot    () { static const char value_scl = '.'; return value_scl; }
      static char minus  () { static const char value_scl = '-'; return value_scl; }
      static char ten    () { static const char value_scl = static_cast< char >( 10 ); return value_scl; }
      static char zero   () { static const char value_scl = '0'; return value_scl; }
      static char space  () { static const char value_scl = ' '; return value_scl; }
      static char tab    () { static const char value_scl = '\t'; return value_scl; }
      static char eol    () { static const char value_scl = '\n'; return value_scl; }
   };
   // ------------------------------------------------------------------------------------------------------

   template<>
   class consts< wchar_t > {
   public:
      static wchar_t a      () { static const wchar_t value_scl = L'a'; return value_scl; }
      static wchar_t A      () { static const wchar_t value_scl = L'A'; return value_scl; }
      static wchar_t dot    () { static const wchar_t value_scl = L'.'; return value_scl; }
      static wchar_t minus  () { static const wchar_t value_scl = L'-'; return value_scl; }
      static wchar_t ten    () { static const wchar_t value_scl = static_cast< wchar_t >( 10 ); return value_scl; }
      static wchar_t zero   () { static const wchar_t value_scl = L'0'; return value_scl; }
      static wchar_t space  () { static const wchar_t value_scl = L' '; return value_scl; }
      static wchar_t tab    () { static const wchar_t value_scl = L'\t'; return value_scl; }
      static wchar_t eol    () { static const wchar_t value_scl = L'\n'; return value_scl; }
   };
   // ------------------------------------------------------------------------------------------------------

   template <>
   class consts < std::string > {
   public:
      static const std::string & trueTxt   () { static const std::string value_scl = "true"; return value_scl; }
      static const std::string & falseTxt  () { static const std::string value_scl = "false"; return value_scl; }
   };
   // ------------------------------------------------------------------------------------------------------

   template <>
   class consts < std::wstring > {
   public:
      static const std::wstring & trueTxt   () { static const std::wstring value_scl = L"true"; return value_scl; }
      static const std::wstring & falseTxt  () { static const std::wstring value_scl = L"false"; return value_scl; }
   };
   // ------------------------------------------------------------------------------------------------------
}; // class convertors
// ---------------------------------------------------------------------------------------------------------

class functors 
{
public:
   functors () = delete;
   
   template < class variable_t, variable_t const_value >
   class set {
   public:
      void operator () ( variable_t & variable_rp ) const  { variable_rp = const_value; }
   };
   // ------------------------------------------------------------------------------------------------------

   template < class out_t, class convertor_t = convertors::simple< out_t > >
   class summator {
   public:
      template < class in_t >
      const out_t & operator () ( out_t & value_cp, const in_t & addition_cp ) const 
      {  return value_cp += convertor_t()( addition_cp ); }
   };
   // ------------------------------------------------------------------------------------------------------

   template < class out_t, out_t range_value, class convertor_t = convertors::simple< out_t > >   
   class left_shift {
   public:
      template < class in_t >
      const out_t & operator () ( const in_t & addition_cp, out_t & value_p ) const 
      {  return value_p = ( value_p * range_value ) + convertor_t()( addition_cp ); }
   };
   // ------------------------------------------------------------------------------------------------------

   template < class container_t, class convertor_t >
   class push_back {
   public:
      template < class in_t >
      const container_t & operator () ( const in_t & element_cp, container_t & value_rp ) const {  
         value_rp.push_back( convertor_t()( element_cp ) ); 
         return value_rp;
      }
   };
   // ------------------------------------------------------------------------------------------------------
}; // class functors {
// ---------------------------------------------------------------------------------------------------------
} // namespace utl {
// ---------------------------------------------------------------------------------------------------------
#endif // UTL_FUNCTORS_H_MRV
// ---------------------------------------------------------------------------------------------------------