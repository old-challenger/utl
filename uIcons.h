// ---------------------------------------------------------------------------------------------------------
#ifndef UTL_ICONS_H_MRV
#define UTL_ICONS_H_MRV
// ---------------------------------------------------------------------------------------------------------
/*!   \file       uIcons.h
      \author     MRV ( mrv.work.box@yandex.ru )
      \date       2013
      \version    0.2
      \brief      
      \copyright  GNU Public License
*/
// ---------------------------------------------------------------------------------------------------------
#include <string>
// ---------------------------------------------------------------------------------------------------------
#include <QtGlobal>
#include <QPixmap>
#include <QIcon>
// ---------------------------------------------------------------------------------------------------------
#include <cassert>
#include <limits>
// ---------------------------------------------------------------------------------------------------------
namespace utl {
// ---------------------------------------------------------------------------------------------------------

class icon
{
private:
	const unsigned char *   data_m; ///< 
	std::size_t				   size_m; ///< 
	std::string				   type_m; ///< 
	// ------------------------------------------------------------------------------------------------------
public:		

   static QPixmap createPixmap ( const icon & icon_cp )
   {
      QPixmap result_l;
      result_l.loadFromData( icon_cp.data(), icon_cp.size< uint >(), icon_cp.type().c_str() );
      return result_l;
   }
   // ------------------------------------------------------------------------------------------------------

   static QIcon createIcon ( const icon & icon_cp ) 
   {  return static_cast< QIcon >( createPixmap( icon_cp ) ); }
   // ------------------------------------------------------------------------------------------------------

   static QIcon createIcon ( const icon & icon1_cp, const icon & icon2_cp )
   {
      QIcon icon_l( createIcon( icon1_cp ) );
      icon_l.addPixmap( createPixmap( icon2_cp ) );
      return icon_l;
   }
   // ------------------------------------------------------------------------------------------------------

   static QIcon createIcon ( const icon & icon1_cp, const icon & icon2_cp, const icon & icon3_cp )
   {
      QIcon icon_l( createIcon( icon1_cp, icon2_cp ) );
      icon_l.addPixmap( createPixmap( icon3_cp ) );
      return icon_l;
   }  
   // ------------------------------------------------------------------------------------------------------

   static QIcon createIcon (  const icon & icon1_cp, const icon & icon2_cp, 
                              const icon & icon3_cp, const icon & icon4_cp )
   {
      QIcon icon_l( createIcon( icon1_cp, icon2_cp, icon3_cp ) );
      icon_l.addPixmap( createPixmap( icon4_cp ) );
      return icon_l;
   }
   // ------------------------------------------------------------------------------------------------------

	icon ( const unsigned char * data_p, std::size_t size_p, const std::string & type_p = "" ) :
		data_m( data_p ),
		size_m( size_p ),
		type_m( type_p )
	{ }
	// ------------------------------------------------------------------------------------------------------
	
	const unsigned char * data () const { return data_m; }
	// ------------------------------------------------------------------------------------------------------
	
   template < class out_type > out_type size () const {
      assert( std::numeric_limits< out_type >::max() >= size_m );
      return static_cast< out_type >( size_m ); 
   }
	// ------------------------------------------------------------------------------------------------------
	
	const std::string & type () const { return type_m; }
	// ------------------------------------------------------------------------------------------------------
};	// class icon
// ---------------------------------------------------------------------------------------------------------
} // namespace utl
// ---------------------------------------------------------------------------------------------------------
#endif // UTL_ICONS_H_MRV
// ---------------------------------------------------------------------------------------------------------