// ---------------------------------------------------------------------------------------------------------
#ifndef UTL_TYPES_H_MRV
#define UTL_TYPES_H_MRV
// ---------------------------------------------------------------------------------------------------------
/*!   \file		   uTypes.h
      \author	   MRV (mrv.work.box@yandex.ru)
      \date		   26.02.2017
      \version	   0.21
      \brief	   Полезные типы данных.
      \copyright  GNU Public License
*/
// ---------------------------------------------------------------------------------------------------------
#include <uFunctions.h>

#include <set>
#include <list>
#include <string>
#include <cassert>
// ---------------------------------------------------------------------------------------------------------

namespace utl {

/// "Пустой" тип данных - вспомогательный тип для ряда обобщённых кострукций библиотеки UTL.
class null_type { };

/*!   Макрос, содержащий определение классов и типов для подбора типов данных по условию
      
      В качестве условия используется оператор сравнения. Тип подбирается перебором типов из списков 
      типов types::lists и сравлением размера каждого типа с заданным размером типа заданным оператором 
      сравнения.

      [!!!] Подумать над перенносом функционала из макросов в обобщеные конструкции
      Для задания условия сравнения размеров типов попробовать использовать лямбда-функции или
      константные выражения.
*/
#define TYPE_BY_CONDITION_CLASS( CLASS_NAME, CONDITION_OPERATOR ) class CLASS_NAME { private:         \
   template< bool condition_value, class typesList_element, std::size_t type_size > class condition;  \
   template< class typesList_element, std::size_t type_size >                                         \
   class condition< false, typesList_element, type_size > { public: typedef typename condition<       \
   type_size CONDITION_OPERATOR sizeof( typename typesList_element::next::type ),                     \
   typename typesList_element::next, type_size >::type type; };                                       \
   template< class typesList_element, std::size_t type_size >                                         \
   class condition< true, typesList_element, type_size > { public:                                    \
   typedef typename typesList_element::type type; };                                                  \
   public: template< std::size_t type_size > class as_unsigned { public: typedef typename condition<  \
   type_size CONDITION_OPERATOR sizeof( typename lists::unsigned_types::type ),                       \
   lists::unsigned_types, type_size >::type type; };                                                  \
   template< std::size_t type_size > class as_signed { public: typedef typename condition<            \
   type_size CONDITION_OPERATOR sizeof( typename lists::signed_types::type ),                         \
   lists::signed_types, type_size >::type type; };                                                    \
   template< std::size_t type_size > class as_float { public: typedef typename condition<             \
   type_size CONDITION_OPERATOR sizeof( typename lists::float_types::type ),                          \
   lists::float_types, type_size >::type type; }; };

/*!   \brief Класс, объединяющий операций с типами данных

   Имя класса используется как пространство имен, объединяющее набор операций с типами и списками типов.
*/
class types {
public:
   types () = delete; // класс не предназначен для создания объектов

   /*!   \brief Класс, определяющий списки типов

         Реализованы четыре списка, содержащие: целочисленные беззнаковые типы, целочисленные знаковые типы,
      типы с плавающей точкой, строковые типы.
   */
   class lists {
   public:
      lists () = delete; // класс не предназначен для создания объектов

      /*!   \brief Шаблонный класс, реализующий элемент списка типов
            \tparam current_type - тип данных, относящийся к даному элементу
            \tparam next_elemet - следующий элемент (собственно реализация связанного списка)
      */
      template< class current_type, class next_elemet >
      class element {
      public:
         element () = delete; // класс не предназначен для создания объектов
         typedef current_type type; ///< Тип данных, относящийся к даному элементу
         typedef next_elemet  next; ///< Следующий элемент списка
      };

      /// Список беззнаковых целых типов данных
      typedef  element< unsigned char, element< unsigned short int, element< unsigned int, 
               element< unsigned long int, element< unsigned long long int, null_type > > > > > unsigned_types;

      /// Список знаковых целых типов данных
      typedef  element < char, element < short int, element < int, element< long int, 
               element< long long int, null_type > > > > > signed_types;

      /// Список типов данных с плавающеё точкой
      typedef  element< float, element< double, element< long double, null_type > > > float_types;

      /// Список строковых типов 
      typedef  element< std::string, element< std::wstring, null_type > > string_types;
   }; // class lists { 

   /*!   \brief Группа классов для подбора типов данных по заданному условию
      
         Для подбора используются списки типов utl::types_lists. Возможен подбор знаковых, беззнаковых 
      целых типов и типов с плавающей точкой.
   */
   class get {
   public:
      get () = delete; // класс не предназначен для создания объектов

      /* \brief Подбор типов, размер которых равен заданному числу байт
      
         Содержит кострукции для определения трёх типов: беззнакового, знакового и с плавающей точкой.
         Подбор типа выполняется по условию равенства размера типа заданному числу байт, пример: 
         get_type::equal< 4 >::as_float::type - тип с плавающей точкой размером равным 4 байтам. 
         При отсутствии типа, удовлетворяющего условию возникает ошибка компиляции.
      */
      TYPE_BY_CONDITION_CLASS( equal, == )

      /* \brief Подбор типов, размер которых меньше или равен заданному числу байт
      
         Содержит кострукции для определения трёх типов: беззнакового, знакового и с плавающей точкой.
         Подбор типа выполняется по условию превышения или равенства размера типа заданному числу байт, 
         пример: 
            get_type::proportional< 3 >::as_signed::type - знаковый целочисленный тип размером больше или 
         равным 3-м байтам. При отсутствии типа, удовлетворяющего условию возникает ошибка компиляции.
      */
      TYPE_BY_CONDITION_CLASS( proportional, <= )
   }; // class get {

   /*!   \brief   Шаблонный класс для проверки типов данных 
         \tparam  typeForCheck - проверяемый тип данных
   */
   template< class typeForCheck >
   class check {
   private:
      check () = delete;

      /*!   \brief Шаблонный клас, реализующий итеративный поиск типа данных в списке типов
            \tparam currentType
            \tparam typesList_element
      */
      template< class currentType, class typesList_element > class iterator {
      public:
         static const bool FOUND = iterator< currentType, typename typesList_element::next >::FOUND;
      };

      /// Отрицательный результат поиска типа в списке типов
      template <>
      class iterator < typeForCheck, null_type > {
      public:
         static const bool FOUND = false;
      };

      /// Положительный результат поиска типа в списке типов
      template < class typesList_element >
      class iterator < typename typesList_element::type, typesList_element > {
      public:
         static const bool FOUND = true;
      };
   public:
      // константы принадлежности проверяемого типа определённым спискам типов
      static const bool IS_SIGNED   = iterator< typeForCheck, lists::signed_types >::FOUND,  ///< знаковый
                        IS_UNSIGNED = iterator< typeForCheck, lists::unsigned_types >::FOUND,///< беззнаковый
                        IS_FLOAT    = iterator< typeForCheck, lists::float_types >::FOUND,   ///< с плавающей точкой
                        IS_STRING   = iterator< typeForCheck, lists::string_types >::FOUND;  ///< строковый

      /*!   \brief Шаблонный класс для проверки на соответствие двух типов
            \tparam typeForCompare - тип с которым производится сравнение

         По-умолчанию результат сравнения отрицательный
      */
      template < class typeForCompare > class compare {
      public:
         static const bool IS_IDENTICAL = false;
      };

      /// Положительный результат сравнения
      template <>
      class compare < typeForCheck > {
      public:
         static const bool IS_IDENTICAL = true;
      };
   }; // template< class typeForCheck > class check {

   /// Идентификаторы типов данных
   enum ids_enum : short unsigned int {
      TYPES_IDS_INCOMPATIBLE = 0x0000, ///< Константа для неизвестного типа данных
      TYPES_IDS_SIMPLE = 0x0100,       ///< Флаг для группы простых типов
      TYPES_IDS_INTEGER = 0x0200,      ///< Флаг для группы целочисленных типов
      TYPES_IDS_SIMPLE_INTEGER = TYPES_IDS_SIMPLE | TYPES_IDS_INTEGER, ///< Простые целочисленные типы 
      TYPES_IDS_SIGNED = 0x0400,       ///< Флаг для группы знаковых типов
      TYPES_IDS_SIGNED_INTEGER = TYPES_IDS_INTEGER | TYPES_IDS_SIGNED, ///< Целочисленные знаковые типы 
      TYPES_IDS_SIMPLE_SIGNED_INTEGER = TYPES_IDS_SIMPLE | TYPES_IDS_SIGNED_INTEGER, ///< Простые целочисленные знаковые типы 
      TYPE_ID_BOOL = TYPES_IDS_SIMPLE | 0x0001, ///< Идентификатор типа bool
      TYPE_ID_CHAR = TYPES_IDS_SIMPLE_SIGNED_INTEGER | 0x0001, ///< Идентификатор типа char
      TYPE_ID_UCHAR = TYPES_IDS_SIMPLE_INTEGER | 0x0002, ///< Идентификатор типа unsignde char
      TYPE_ID_SHORT_INT = TYPES_IDS_SIMPLE_SIGNED_INTEGER | 0x0003, ///< Идентификатор типа short int
      TYPE_ID_SHORT_UINT = TYPES_IDS_SIMPLE_INTEGER | 0x0004, ///< Идентификатор типа unsigned short int
      TYPE_ID_INT = TYPES_IDS_SIMPLE_SIGNED_INTEGER | 0x0005, ///< Идентификатор типа int
      TYPE_ID_UINT = TYPES_IDS_SIMPLE_INTEGER | 0x0006, ///< Идентификатор типа unsigned int
      TYPE_ID_LONG_INT = TYPES_IDS_SIMPLE_SIGNED_INTEGER | 0x0007, ///< Идентификатор типа long int
      TYPE_ID_LONG_UINT = TYPES_IDS_SIMPLE_INTEGER | 0x0008, ///< Идентификатор типа unsigned long int
      TYPE_ID_LONG_LONG_INT = TYPES_IDS_SIMPLE_SIGNED_INTEGER | 0x0009, ///< Идентификатор типа long long int
      TYPE_ID_LONG_LONG_UINT = TYPES_IDS_SIMPLE_INTEGER | 0x000a, ///< Идентификатор типа unsigned long long int
      TYPES_IDS_FLOAT_DOT = 0x0800, ///< Флаг для группы типов с плавающей точкой
      TYPES_IDS_SIMPLE_FLOAT_DOT = TYPES_IDS_SIMPLE | TYPES_IDS_FLOAT_DOT, ///< Простые типы с плавающей точкой
      TYPE_ID_FLOAT = TYPES_IDS_SIMPLE_FLOAT_DOT | 0x0001, ///< Идентификатор типа float
      TYPE_ID_DOUBLE = TYPES_IDS_SIMPLE_FLOAT_DOT | 0x0002, ///< Идентификатор типа double
      TYPE_ID_LONG_DOUBLE = TYPES_IDS_SIMPLE_FLOAT_DOT | 0x0003, ///< Идентификатор типа long double
      //
      TYPES_IDS_STRINGS = 0x1000, ///< Флаг для группы строковых типов
      TYPE_ID_STL_STRING = TYPES_IDS_STRINGS | 0x0001, ///< Идентификатор типа std::string
      TYPE_ID_STL_WSTRING = TYPES_IDS_STRINGS | 0x0002 ///< Идентификатор типа std::string
   }; // enum typesIds_enum : id16_t {
}; // class types {

typedef std::size_t              id_t;          ///< числовой тип используемый для идентификаторов и индексов
typedef std::set< id_t >         ids_set;       ///< множество идентификаторов
typedef std::list< id_t >        ids_list;
typedef std::set< std::string >  strings_set;   ///< множество строк
typedef std::list< std::string > strings_list;  ///< список строк
typedef types::get::equal::as_unsigned< 1 >::type id8_t;   ///< тип для идентификаторов размером в один байт
typedef types::get::equal::as_unsigned< 2 >::type id16_t;  ///< тип для идентификаторов размером в два байта
typedef types::get::equal::as_unsigned< 4 >::type id32_t;  ///< тип для идентификаторов размером в четыре байта
typedef types::get::equal::as_unsigned< 8 >::type id64_t;  ///< тип для идентификаторов размером в восемь байт
// ---------------------------------------------------------------------------------------------------------

/*!   \brief Шаблонный класс для получения значения идентификатора типа 
      \tparam some_type - тип для которого определяется идентификатор

      Константа VALUE равна идентификатору типа some_type. Если тип не поддерживается 
      VALUE = TYPES_IDS_INCOMPATIBLE.
*/
template < class some_type > 
class type2id { 
public: 
   enum : id16_t { 
      VALUE = TYPES_IDS_INCOMPATIBLE ///< Значение идентификатора типа по-умолчанию
   }; 
};

/*!   \brief Шаблонный класс получения типа по его идентификатору
      \tparam id_const - идентификатор и скомого типа из перечисления typesIds_enum

      Искомый тип определется лексемой is. Если тип не поддерживается - лексема is определена не будет.
*/
template < id16_t id_const > class id2type;

/// Макрос определяющий реализации шаблонных класов type2id и id2type для конктетной пары (тип, идентификатор)
#define ASSIGN_TYEPE2ID( TYPE, ID ) \
   template <> class type2id < TYPE > { public: enum : unsigned short int { VALUE = ID }; }; \
   template <> class id2type< ID > { public: typedef TYPE is; };

// Привязка типов к идентификаторам
ASSIGN_TYEPE2ID( bool,                    types::TYPE_ID_BOOL           )
ASSIGN_TYEPE2ID( char,                    types::TYPE_ID_CHAR           )
ASSIGN_TYEPE2ID( unsigned char,           types::TYPE_ID_UCHAR          )
ASSIGN_TYEPE2ID( short int,               types::TYPE_ID_SHORT_INT      )
ASSIGN_TYEPE2ID( unsigned short int,      types::TYPE_ID_SHORT_UINT     )
ASSIGN_TYEPE2ID( int,                     types::TYPE_ID_INT            )
ASSIGN_TYEPE2ID( unsigned int,            types::TYPE_ID_UINT           )
ASSIGN_TYEPE2ID( long int,                types::TYPE_ID_LONG_INT       )
ASSIGN_TYEPE2ID( unsigned long int,       types::TYPE_ID_LONG_UINT      )
ASSIGN_TYEPE2ID( long long int,           types::TYPE_ID_LONG_LONG_INT  )
ASSIGN_TYEPE2ID( unsigned long long int,  types::TYPE_ID_LONG_LONG_UINT )
ASSIGN_TYEPE2ID( float,                   types::TYPE_ID_FLOAT          )
ASSIGN_TYEPE2ID( double,                  types::TYPE_ID_DOUBLE         )
ASSIGN_TYEPE2ID( long double,             types::TYPE_ID_LONG_DOUBLE    )
ASSIGN_TYEPE2ID( std::string,             types::TYPE_ID_STL_STRING     )
ASSIGN_TYEPE2ID( std::wstring,            types::TYPE_ID_STL_WSTRING    )
// ---------------------------------------------------------------------------------------------------------

/*!   \brief Количество байт по заданному количеству бит

   Значение размещается в константе AMOUNT.
*/
template < id8_t bits_amount > 
class bits2bytes { 
public: 
   enum : id8_t { 
      AMOUNT = ( ( bits_amount - 1 ) >> 3 ) + 1
   }; 
};
template <> class bits2bytes< 0 > { public: enum : id8_t { AMOUNT = 0 }; };

/*!   \brief Количество бит по заданному количеству байт

   Значение размещается в константе AMOUNT.
*/
template < id8_t bytes_amount > class bytes2bits { public: enum : id8_t { AMOUNT = bytes_amount << 3 }; };

/*!   \brief   Шаблонный клас предназначенный для генерации маски из заданного количества битовых единиц
      \tparam  integer_type - тип генерируемого значения

*/
template < class integer_type > 
class mask {
public:
   // Проверка корректности использования типа в параметре шаблона
   static_assert( std::numeric_limits< integer_type >::is_integer, 
                  "[ERROR] Incompatible type in template parameter \"integer_type\"." );

   /*!   \brief   Шаблонных класс, генерирующий константное значение 
         \tparam  mask_size - кличество битовых единиц в генерируемой маске

         Значение размещается в константе VALUЕ.
   */
   template < id8_t mask_size >
   class constant {
   public:
      static_assert( bytes2bits< sizeof( integer_type ) >::AMOUNT >= mask_size,
                     "[ERROR] Using type integer_type is too small for required bits amount mask_size." );
      enum : integer_type {
         VALUE = integer_type( mask< integer_type >::constant< mask_size - 1 >::VALUE << 1 )
            | mask< integer_type >::constant< 1 >::VALUE
      };
   };

   template <> class constant< 0 > { public: enum : integer_type { VALUE = 0x0 }; };

   template <> class constant< 1 > { public: enum : integer_type { VALUE = 0x1 }; };

   /*!   \brief   Шаблонный статический метод для вычисления 
   */
   template < class another_integer_type >
   static integer_type compute( another_integer_type maskSize_p ) {
      // Проверка корректности использования типа в параметре шаблона
      static_assert( std::numeric_limits< another_integer_type >::is_integer,
                     "[ERROR] Incompatible type in template parameter \"integer_type\"." );
      assert( bytes2bits< sizeof( integer_type ) >::AMOUNT >= maskSize_p
         && "[ERROR] Using type integer_type is too small for required bits amount maskSize_p." );
      return bytes2bits< sizeof( integer_type ) >::AMOUNT == maskSize_p
         ? static_cast< integer_type >( -1 ) : ( static_cast< integer_type >( 1 ) << maskSize_p ) - 1;
   }
}; // template < class integer_type > class mask {
// ---------------------------------------------------------------------------------------------------------

/*!   \brief   Представление плоского типа данных в исходном виде и в виде байтового массива

   Определяется тип данных form, размер которого равен размеру исхоного типа данных
*/
template< class flat_type >
class dual {
public:
   /*!   \brief Количество байт занимаемых типом float_type
         \return Константное значение sizeof( float_type )
   */
   static std::size_t size () {
      return sizeof( flat_type );
   }
   
   /*! \brief Конструктор по-умолчанию (значение по-умолчанию .0)
   */
   dual () { value_m.source = .0; }

   /*! \brief Основной конструктор 
   */
   dual ( flat_type value_p ) { value_m.source = value_p; }

   /*! \brief Конструктор копирования
   */
   dual ( const dual & etalon_cp ) { value_m.source = etalon_cp.value_m.source; }

   /*! \brief Изначальное значение с плавающей точкой
   */
   const flat_type & source () const {
      return value_m.source;
   }

   /*! \brief Изначальное значение с плавающей точкой
   */
   const id8_t * bytes () const {
      return value_m.bytes;
   }

   /*! \brief Оператор сравнения
   */
   bool operator == ( const dual< flat_type > & etalon_p ) const {
      return value_m.source == etalon_p.value_m.source;
   }
private:
   union {
      flat_type   source;                       ///< В исходном виде
      id8_t       bytes[ sizeof( flat_type ) ]; ///< В виде байтового массива
   } value_m;
}; // template< class flat_type > class dual {

/*!   \brief   Класс для разбора числа с плавающей точкой 
      \tparam  float_type - тип с плавающей точкой разбираемый данных
      
   Класс, выполняющий разбор числа с плавающей точкой. Мантиса и порядок разделяются и выводятся отдельно.
*/
template< class float_type >
class floatBits : public dual< float_type > {
public:
   // Проверка на использование типа с плавающей точкой
   static_assert( types::check< float_type >::IS_FLOAT,
                  "[ERROR] Incompatible type in template parameter \"float_type\"." );

   /*!   \brief Константы, определяющие структуру разбираемого типа с плавающей точкой

       <ol>
         <li> MANTISSA_TYPE_SIZE - количество байт, необходимых для размещения мантисы
         <li> MANTISSA_BITS_AMOUNT - количество бит под мантису 
         <li> MANTISSA_BYTES_AMOUNT - количество байт мантисы (может отличаться от MANTISSA_TYPE_SIZE)
         <li> MANTISSA_PREFIX_SIZE - количество бит непоместившихся в целые байты мантисы (размер префикса)
         <li> EXPONENT_BITS_AMOUNT - количество бит под порядок
         <li> EXPONENT_SUFFIX_SIZE - количество бит непоместившихся в целые байты порядка (размер суффикса)
         <li> EXPONENT_BYTES_AMOUNT - количество байт порядка
      </ol>
   */
   enum consts_enum : id8_t {
      MANTISSA_TYPE_SIZE      = bits2bytes< std::numeric_limits< float_type >::digits >::AMOUNT,
      MANTISSA_BITS_AMOUNT    = std::numeric_limits< float_type >::digits - 1,
      MANTISSA_BYTES_AMOUNT   = bits2bytes< MANTISSA_BITS_AMOUNT >::AMOUNT,
      MANTISSA_PREFIX_SIZE    = MANTISSA_BITS_AMOUNT % 8,
      EXPONENT_BITS_AMOUNT    = bytes2bits< sizeof( float_type ) >::AMOUNT 
                                 - std::numeric_limits< float_type >::digits,
      EXPONENT_SUFFIX_SIZE    = ( EXPONENT_BITS_AMOUNT + 1 ) % 8,
      EXPONENT_BYTES_AMOUNT   = bits2bytes< EXPONENT_BITS_AMOUNT - EXPONENT_SUFFIX_SIZE + 1 >::AMOUNT 
                                 + ( EXPONENT_SUFFIX_SIZE ? 1 : 0 )
   };
      
   /// Целочисленный тип для значения мантисы
   typedef typename types::get::proportional::as_unsigned< MANTISSA_TYPE_SIZE >::type mantissa_type;
   
   /// Целочисленный тип для значения порядка
   typedef typename types::get::proportional::as_signed< EXPONENT_BYTES_AMOUNT >::type exponent_type;

   /*!   \brief Число с плавающей точкой, разобранное по частям  

         Результат работы метода floatBits::split(). Определение составных частей числа выполняется методом. 
      Класс предназначен только для хранения и корректной передач данных в виде единого объекта.
   */
   class parts {
   public:
      /*!   \brief Конструктор по умолчанию

         Используется только в автоматическом режиме (например, при формировании элеметов контейнера)   
      */
      parts () : sign_m( true ), aliquot_m( 0 ), fraction_m( 0 ), exponent_m( 0 ) { ; }
      
      
      /*!   \brief Конструктор, собирающий части числа с плавающей точкой в единый объект

         Основной конструктор для создания оъектов
      */
      parts ( bool sign_p, mantissa_type aliquot_p, mantissa_type fraction_p, exponent_type exponent_p ) : 
         sign_m( sign_p ), aliquot_m( aliquot_p ), fraction_m( fraction_p ), exponent_m( exponent_p ) { ; }
      
      
      /*!   \brief Конструктор копирования
      */
      parts ( const parts & etalon_cp ) : 
         sign_m( etalon_cp.sign_m ), aliquot_m( etalon_cp.aliquot_m ), fraction_m( etalon_cp.fraction_m ), 
         exponent_m( etalon_cp.exponent_m ) { ; }

      /*!   \brief   Знак числа с плавающей точкой
            \return  Значение типа bool
      */
      bool           sign     () const { return sign_m;     }

      /*!   \brief   Целая часть числа с плавающей точкой
            \return  Значение типа mantissa_type
      */
      mantissa_type  aliquot  () const { return aliquot_m;  }

      /*!   \brief   Дробная часть числа с плавающей точкой
            \return  Значение типа mantissa_type
      */
      mantissa_type  fraction () const { return fraction_m; }

      /*!   \brief   Двоичный порядок числа с плавающей точкой
            \return  Значение типа exponent_type
      */
      exponent_type  exponent () const { return exponent_m; }
   private:
      bool           sign_m;     ///< знак числа 
      mantissa_type  aliquot_m,  ///< битовое представление целой части числа 
                     fraction_m; ///< битовое представленеи дробной части числа
      exponent_type  exponent_m; ///< двоичный порядок числа
   }; // class parts {

   /*!   \brief Количество байт занимаемых типом float_type
         \return Константное значение sizeof( float_type )
   */
   static std::size_t size () {
      return sizeof( float_type );
   }

   /*! \brief Конструктор по-умолчанию (значение по-умолчанию .0)
   */
   floatBits () : dual() { ; }

   /*! \brief Основной конструктор 
   */
   floatBits ( float_type value_p ) : dual( value_p ) { ; }

   /*! \brief Конструктор копирования
   */
   floatBits ( const floatBits & etalon_cp ) : dual( etalon_cp ) { ; }

   /*!   \brief Целочисленное знаечние порядка
         \return Значение порядка типа exponent_type
   */
   exponent_type exponent () const {
      static const std::size_t 
         exponentBytesOffset_scl = MANTISSA_BYTES_AMOUNT - ( MANTISSA_PREFIX_SIZE ? 1 : 0 );
      static const exponent_type 
         exponentMask_scl = mask< exponent_type >::constant< bytes2bits< sizeof( exponent_type ) >::AMOUNT - 1 >::VALUE,
         subtrahend_scl = mask< exponent_type >::constant< EXPONENT_BITS_AMOUNT - 1 >::VALUE;
      exponent_type result_l = 0;
      memcpy( &result_l, bytes() + exponentBytesOffset_scl, EXPONENT_BYTES_AMOUNT );
      result_l &= exponentMask_scl;
      result_l >>= MANTISSA_PREFIX_SIZE;
      result_l -= subtrahend_scl;
      return result_l;
   }

   /*!   \brief "Отрицательность" числа с плавающей точкой 
         \return true - число отрицательное, false - число положительное
   */
   bool isNegative() const {
      static const auto signByteNumber_scl = sizeof( float_type ) - 1;
      static const id8_t hiBitMask_scl = '\x80';
      return hiBitMask_scl & bytes()[ signByteNumber_scl ];
   }

   /*!   \brief "Положительность" числа с плавающей точкой 
         \return true - число положительное, false - число отрицательное
   */
   bool isPositive() const {
      return !isNegative();
   }

   /*!   \brief Целочисленное знаечние мантисы (включая нехранимый первый единичный бит)
         \return Значение мантисы типа mantissa_type
   */
   mantissa_type mantissa () const {
      static const auto hiBitMask_scl = mask< mantissa_type >::constant< 1 >::VALUE << MANTISSA_BITS_AMOUNT;
      static const auto mantissaMask_scl = mask< mantissa_type >::constant< MANTISSA_BITS_AMOUNT >::VALUE;
      mantissa_type result_l = 0;
      memcpy( &result_l, bytes(), MANTISSA_BYTES_AMOUNT );
      result_l = hiBitMask_scl | ( mantissaMask_scl & result_l );
      return result_l;
   }

   /*!   \brief Знак числа с плавающей точкой 
         \return Единичное значение с соответствующим знаком привидёное к типу sign_type.

         Тип возвращаемого значения должен быть знаковым целым, в противном случае возникает ошибка 
      компиляции. Для типа данных bool метод sign() реализован отдельно true - число положительное, 
      false - число отрицательное.
   */
   template < class sign_type >
   sign_type sign () const {
      static_assert( types::check< sign_type >::IS_SIGNED, "[ERROR] Output type is not signed integer." );
      return isNegative() ? static_cast< sign_type >( -1 ) : static_cast< sign_type >( 1 );
   }

   template <> bool sign< bool >() const { return isPositive(); }

   /*!   \brief   Выделение отдельных частей числа с плвающей точкой: целой и дробной частей и порядка
         \return  Объект класа floatParts содержащий все части числа с плавающей точкой

         !!! Функция не реализована !!!
   */
   parts split () const {
      static const exponent_type 
         noExponentAliquotLimit_scl = static_cast< exponent_type >( 
            bits2bytes< sizeof( mantissa_type ) >::AMOUNT - 1 ),
         noExponentFractionLimit_scl = static_cast< exponent_type >( 
            bits2bytes < sizeof( mantissa_type ) >::AMOUNT - MANTISSA_BITS_AMOUNT );
      exponent_type  exponent_l = exponent();
      const mantissa_type fractionBitsAmountInMantissa_cl = MANTISSA_BITS_AMOUNT - exponent_l;
      const mantissa_type  
         aliquotBitsAmount_cl = exponent_l < 0 
            ? 0 : exponent_l < noExponentAliquotLimit_scl ? static_cast< mantissa_type >( exponent_l ) : 1,
         fractionBitsAmount_cl = exponent_l > MANTISSA_BITS_AMOUNT 
            ? 0 : exponent_l > noExponentFractionLimit_scl 
            ? fractionBitsAmountInMantissa_cl : MANTISSA_BITS_AMOUNT,
         mantissa_cl = mantissa(),
         aliquot_cl = aliquotBitsAmount_cl ? aliquotBitsAmount_cl < MANTISSA_BITS_AMOUNT ? 
            mantissa_cl >> ( MANTISSA_BITS_AMOUNT - aliquotBitsAmount_cl ) : 
            mantissa_cl << ( aliquotBitsAmount_cl - MANTISSA_BITS_AMOUNT ) : 0, 
         fraction_cl = fractionBitsAmount_cl 
            ? mask< mantissa_type >::compute( fractionBitsAmount_cl ) & mantissa_cl : 0;
      if ( aliquotBitsAmount_cl == exponent_l || fractionBitsAmount_cl == fractionBitsAmountInMantissa_cl ) {
         //assert(     ( aliquotBitsAmount_cl == exponent_l && !fractionBitsAmount_cl )
         //        ||  ( fractionBitsAmount_cl == fractionBitsAmountInMantissa_cl && !aliquotBitsAmount_cl ) );
         exponent_l = 0;
      }
      return parts( sign< bool >(), aliquot_cl, fraction_cl, exponent_l );
   }
   /*! \brief Оператор сравнения
   
   bool operator == ( const floatBits< float_type > & etalon_p ) const {
      return value.source_m == etalon_p.value.source_m;
   } /**/
}; // class floatBits {
// ---------------------------------------------------------------------------------------------------------

/*!   \brief Шаблонный класс двумерной точки
      \tparam elemtary_type - тип данных для представления координат точки
*/
template < class elemtary_type >
class point2D {
public:
   ///   \brief Конструктор по-умолчанию (значения координат обнуляются)
   point2D () : x_m( 0 ), y_m( 0 ) { ; }

   ///   \brief Конструктор копирования
   point2D ( const point2D & etalon_cp ) : x_m( etalon_cp.x_m ), y_m( etalon_cp.y_m ) { ; }

   ///   \brief Конструктор с заданием значению координат точки
   point2D ( const elemtary_type & x_cp, const elemtary_type & y_cp ) : x_m( x_cp ), y_m( y_cp ) { ; }
   
   ///   \brief Константное значение координаты X
   const elemtary_type & x () const { return x_m; }

   ///   \brief Константное значение координаты Y
   const elemtary_type & y () const { return y_m; }
   
   /*!   \brief Метод для задания значения координаты X
         \param value_cp - значение координаты
   */
   void setX ( const elemtary_type & value_cp ) { x_m = value_cp; }

   /*!   \brief Метод для задания значения координаты Y
         \param value_cp - значение координаты
   */
   void setY ( const elemtary_type & value_cp ) { y_m = value_cp; }
   
   /*!   \brief Опреатор присваивания
         \param value_cp - присваиваемое значение
   */
   const point2D & operator = ( const point2D & value_cp ) {
      x_m = value_cp.x_m; 
      y_m = value_cp.y_m;
      return *this;
   }
   // ------------------------------------------------------------------------------------------------------
private:
   elemtary_type  x_m, ///< координата X
                  y_m; ///< координата Y
   // ------------------------------------------------------------------------------------------------------
}; // class point2D
// ---------------------------------------------------------------------------------------------------------

/*!   \brief Шаблонный класс-реализация геометрического вектора
      \tparam point_type - тип, используемый для представления точек (начало и конец вектора)

      В версии 0.2 и раннее - шаблонный класс rectangle.
*/
template < class point_type >
class arrow {
public:
   /// Конструктор по-умолчанию
   arrow () : begin_m(), end_m() { ; }
   
   /*!   \brief Конструктор копирования
         \param etalon_cp - копируемый вектор
   */
   arrow ( const arrow & etalon_cp ) : begin_m( etalon_cp.begin_m ), end_m( etalon_cp.end_m ) { ; }
   
   /*!   \brief Конструктор копирования
         \param begin_cp - начало вектора
         \param end_cp - конец вектора
   */
   arrow ( const point_type & begin_cp, const point_type & end_cp ) : 
      begin_m( begin_cp ), end_m( end_cp ) 
   { ; }
   
   /// Константное значение начала вектора
   const point_type & begin () const { return begin_m; }
   
   /// Константное значение конца вектора
   const point_type & end () const { return end_m; }
   
   /*!   \brief Метод для установки начала вектора
         \param point_cp - устанавливаемое значение тотчки
   */
   void setBegin ( const point_type & point_cp ) { begin_m = point_cp; }
   
   /*!   \brief Метод для установки конца вектора
         \param point_cp - устанавливаемое значение тотчки
   */
   void setEnd ( const point_type & point_cp ) { end_m = point_cp; }
   
   /*!   \brief Оператор присваивания
         \param value_cp - присваиваемое значение вектора
   */
   const arrow & operator = ( const arrow & value_cp ) {
      begin_m  = value_cp.begin_m;      
      end_m    = value_cp.end_m; 
      return *this;
   }
private:
   point_type  begin_m, ///< начало вектора 
               end_m;   ///< кнец вектора
   // ------------------------------------------------------------------------------------------------------
}; // class arrow
// ---------------------------------------------------------------------------------------------------------
}; // namespace utl {
// ---------------------------------------------------------------------------------------------------------
#endif // UTL_TYPES_H_MRV
// ---------------------------------------------------------------------------------------------------------
