// ---------------------------------------------------------------------------------------------------------
#ifndef UTL_VARIANT_H_MRV
#define UTL_VARIANT_H_MRV
// ---------------------------------------------------------------------------------------------------------
/*!	\file		   uVariant.h
      \author	   MRV ( mrv.work.box@yandex.ru )
      \date		   26.03.2016
      \version	   0.1
      \brief	   Класс для хранения даных произвольного типа.
      \copyright  GNU Public License
*/
// ---------------------------------------------------------------------------------------------------------
#include <uFunctors.h>
#include <uTypes.h>
#include <vector>
// ---------------------------------------------------------------------------------------------------------

/// Пространство имен библиотеки UTL (Useful Trifles Library)
namespace utl {
// ---------------------------------------------------------------------------------------------------------
typedef unsigned char storing_type; ///< тип элемента массива для хранения данных в классе utl::variant
typedef std::vector< storing_type > storing_array; ///< массива для хранения данных в классе utl::variant

/*!   \brief Класс используемый по-умолчанию для преобразования, сериализации и десериализации типов
*/
class defaultTransformer {
public:

   /*!   \brief   Преобразование данных с исходным типом, определённым идентификатом, из байтового массива 
         \tparam  out_type - тип данных, к которому осуществляется преобразование
         \param   data_cp - массив, содержащий исходные данные
         \param   typeId_p - идентификатор типа данных, размещённых в массиве 
         \return  Данные преобразованные к заданном типу
   */
   template < class out_type >
   out_type cast ( const storing_array & data_cp, id16_t typeId_p ) const {
      if ( type2id< out_type >::VALUE == typeId_p )
         return deserialize< out_type > ( data_cp );
      switch ( typeId_p ) {
         case types::TYPE_ID_BOOL:            return cast< out_type, bool >                   ( data_cp );
         case types::TYPE_ID_CHAR:            return cast< out_type, char >                   ( data_cp );
         case types::TYPE_ID_UCHAR:           return cast< out_type, unsigned char >          ( data_cp );
         case types::TYPE_ID_SHORT_INT:       return cast< out_type, short int >              ( data_cp );
         case types::TYPE_ID_SHORT_UINT:      return cast< out_type, unsigned short int >     ( data_cp );
         case types::TYPE_ID_INT:             return cast< out_type, int >                    ( data_cp );
         case types::TYPE_ID_UINT:            return cast< out_type, unsigned int >           ( data_cp );
         case types::TYPE_ID_LONG_INT:        return cast< out_type, long int >               ( data_cp );
         case types::TYPE_ID_LONG_UINT:       return cast< out_type, unsigned long int >      ( data_cp );
         case types::TYPE_ID_LONG_LONG_INT:   return cast< out_type, long long int >          ( data_cp );
         case types::TYPE_ID_LONG_LONG_UINT:  return cast< out_type, unsigned long long int > ( data_cp );
         case types::TYPE_ID_FLOAT:           return cast< out_type, float >                  ( data_cp );
         case types::TYPE_ID_DOUBLE:          return cast< out_type, double >                 ( data_cp );
         case types::TYPE_ID_LONG_DOUBLE:     return cast< out_type, long double >            ( data_cp );
         case types::TYPE_ID_STL_STRING:      return cast< out_type, std::string >            ( data_cp );
         case types::TYPE_ID_STL_WSTRING:     return cast< out_type, std::wstring >           ( data_cp );
         default:
            assert ( !"[ERROR] Incompatible storing type for variant type" );
            return out_type ();
      }; // switch ( typeId_p ) {
   } // template < class out_t > out_t cast ( 

   /*!   \brief   Десериализация - преобразование из байтового массива к исходному (!) типу данных
         \tparam  out_t - тип данных, к которому выполняется преобразование
         \param   data_cp - байтовый массив, из которого выполняется преобразование
         \return  Значение представленное в исходном типе данных
   */
   template < class out_type >
   out_type deserialize ( const storing_array & data_cp ) const {
      return copier< ( types::TYPES_IDS_SIMPLE | types::TYPES_IDS_STRINGS )
         & type2id< out_type >::VALUE > ().from< out_type >( data_cp );
   }

   /*!   \brief   Сериализация - преобразование из исходного типа к байтовому массиву
         \tparam  in_t - тип данных, из которого выполняется преобразование
         \param   in_cp - константная ссылка на данные, представленные в типе данных in_t
         \param   data_rp - ссылка на байтовый массив, в который выполняется запись данных
   */
   template < class in_type >
   void serialize ( const in_type & in_cp, storing_array & data_rp ) const {
      copier< ( types::TYPES_IDS_SIMPLE | types::TYPES_IDS_STRINGS )
         & type2id< in_type >::VALUE > ().to( in_cp, data_rp );
   }
private:

   /*!   \brief   Класс, содержащий методы копирования данных между байтовым массивом и некоторым типом
         \tparam  inTypeId_const - идентификатор типа для кторого применяются операции
   */
   template < id16_t inTypeId_const > 
   class copier {
   public:

      /*!   \brief   Серриализация - копирование данных к байтовому массиву   
            \tparam  in_type - обрабатываемый тип данных
            \param   in_cp - ссылка на исходный экземпляр данных
            \param   data_rp - ссылка на байтовый массив для записи данных
      */
      template < class in_type > 
      void to ( const in_type & in_cp, storing_array & data_rp );

      /*!   \brief   Десериализация - копирование даных из байтового массива
            \tparam  out_type - обрабатываемый тип данных
            \param   data_cp - ссылка на исходный байтовый массив
      */
      template < class out_type >
      out_type from ( const storing_array & data_cp );
   };

   /*!   \brief   Реализация шаблонного класса utl::defaultTransformer::copier для группы простых типов \
                  utl::TYPES_IDS_SIMPLE (все числовые типы)
   */
   template <>
   class copier< types::TYPES_IDS_SIMPLE > {
   public:
      template < class in_type >
      void to ( const in_type & in_cp, storing_array & data_rp ) {
         static_assert( type2id< in_type >::VALUE & types::TYPES_IDS_SIMPLE,
                        "[ERROR] Incorrect using copier::to() method for some simple type." );
         const storing_type * const begin_cl = ( storing_type * ) &in_cp;
         data_rp.insert ( data_rp.end(), begin_cl, begin_cl + sizeof ( in_type ) / sizeof( storing_type ) );
      }

      template < class out_type >
      out_type from ( const storing_array & data_cp ) { 
         static_assert( type2id< out_type >::VALUE & types::TYPES_IDS_SIMPLE,
                        "[ERROR] Incorrect using copier::from() method for some simple type." );
         return *(( const out_type * ) data_cp.data()); // data_cp.front();
      }
   };
       
   /*!   \brief   Реализация шаблонного класса utl::defaultTransformer::copier для группы стандартных строковых \ 
                  типов utl::TYPES_IDS_STRINGS
   */
   template <>
   class copier< types::TYPES_IDS_STRINGS > {
   public:
      template < class in_type >
      void to ( const in_type & in_cp, storing_array & data_rp ) {
         static_assert( type2id< in_type >::VALUE & types::TYPES_IDS_STRINGS,
                        "[ERROR] Incorrect using copier::to() method for string type." );
         const storing_type * const begin_cl = ( storing_type * ) in_cp.c_str();
         data_rp.insert ( data_rp.end(), begin_cl, 
                          begin_cl + sizeof ( typename in_type::value_type ) * in_cp.length() 
                                       / sizeof( storing_type ) );
      }

      template < class out_type >
      out_type from ( const storing_array & data_cp ) {
         static_assert( type2id< out_type >::VALUE & types::TYPES_IDS_STRINGS,
                        "[ERROR] Incorrect using copier::from() method for string type." );
         return out_type( ( typename out_type::value_type const * ) &data_cp.front(), 
            data_cp.size () / sizeof ( typename out_type::value_type ) * sizeof ( storing_type ) );
      }
   };

   /*!   \brief   Шаблонный класс-функтор для преобразования типов данных
         \tparam  outTypeId_const - идентификатор группы типов, которой принадлежит конечный тип
         \tparam  inTypeId_const - идентификатор группы типов, которой принадлежит исходный тип
   
         Данный шаблон необходим для группировки методов преобразования в зависимости от участвующих 
      в операции типов. Группы типов определены в перечислении utl::typesIds_enum.
   */
   template < id16_t outTypeGroupId_const, id16_t inTypeGroupId_const >
   class caster {
   public:

      /*!   \brief   Шаблонный класс-функтор для преобразования типов данных
            \tparam  out_type - конечный тип
            \tparam  in_type - исходный тип
            \param   data_cp - преобразуемый экземпляр данных
      */
      template < class out_type, class in_type > out_type operator () ( const in_type & data_cp ) const;
   };

   /// Реаизация класса-функтора для преобразования простого типа к простому типу
   template <>
   class caster < types::TYPES_IDS_SIMPLE, types::TYPES_IDS_SIMPLE > {
   public:
      template < class out_type, class in_type >
      out_type operator () ( const in_type & data_cp ) const {
         static_assert( type2id< in_type >::VALUE & types::TYPES_IDS_SIMPLE,
                        "[ERROR] Incorrect using caster class for some simple input type." );
         static_assert( type2id< out_type >::VALUE & types::TYPES_IDS_SIMPLE,
                        "[ERROR] Incorrect using caster class for some simple output type." );
         return convertors::simple< out_type > ()( data_cp );
      }
   };

   /// Реаизация класса-функтора для преобразования простого типа к строковому типу
   template <>
   class caster < types::TYPES_IDS_SIMPLE, types::TYPES_IDS_STRINGS > {
   public:
      template < class out_type, class in_type >
      out_type operator () ( const in_type & data_cp ) const {
         static_assert( type2id< in_type >::VALUE & types::TYPES_IDS_STRINGS,
                        "[ERROR] Incorrect using caster class for string input type." );
         static_assert( type2id< out_type >::VALUE & types::TYPES_IDS_SIMPLE,
                        "[ERROR] Incorrect using caster class for some simple output type." );
         return convertors::str2num< out_type > ()( data_cp );
      }
   };

   /// Реаизация класса-функтора для преобразования строкового типа к простому типу
   template <>
   class caster < types::TYPES_IDS_STRINGS, types::TYPES_IDS_SIMPLE > {
   public:
      template < class out_type, class in_type >
      out_type operator () ( const in_type & data_cp ) const {
         static_assert( type2id< in_type >::VALUE & types::TYPES_IDS_SIMPLE,
                        "[ERROR] Incorrect using caster class for some simple input type." );
         static_assert( type2id< out_type >::VALUE & types::TYPES_IDS_STRINGS,
                        "[ERROR] Incorrect using caster class for string output type." );
         return convertors::num2str< out_type, convertors::empty_base > ()( data_cp );
      }      

   };

   /// Реаизация класса-функтора для преобразования строкового типа к строковому типу
   template <>
   class caster < types::TYPES_IDS_STRINGS, types::TYPES_IDS_STRINGS > {
   public:
      template < class out_type, class in_type >
      out_type operator () ( const in_type & data_cp ) const {
         static_assert( type2id< in_type >::VALUE & types::TYPES_IDS_STRINGS,
                        "[ERROR] Incorrect using caster class for string input type." );
         static_assert( type2id< out_type >::VALUE & types::TYPES_IDS_STRINGS,
                        "[ERROR] Incorrect using caster class for string output type." );
         return convertors::str2str< out_type, convertors::empty_base >()( data_cp );
      }
   };

   /*!   \brief   Реализация преобразования типов через соответствующую реализацию в виде функтора caster
         \tparam  out_type - конечный тип 
         \tparam  in_type - исходный тип
         \param   data_cp - массив, содержащий преобразуемые данные
   */
   template < class out_type, class in_type >
   out_type cast ( const storing_array & data_cp ) const {
      return caster< ( types::TYPES_IDS_SIMPLE | types::TYPES_IDS_STRINGS ) & type2id< out_type >::VALUE,
                     ( types::TYPES_IDS_SIMPLE | types::TYPES_IDS_STRINGS ) & type2id< in_type >::VALUE >().
                        operator()< out_type, in_type >( deserialize< in_type >( data_cp ) );
   }
}; // class defaultTransformer {

/*!   \brief Реализация класса для хранения даных произвольного типа.
   
      Предназначен для хранения данных, преоразованных из различных типов в байтовый массив по алгоритмам, 
   реализованным в классе \b utl::defaultTransformer. Не смотря на то, что данные хранятся в байтовом 
   массиве, их представление зависит от исходного типа. Идентификатор исходного типа так же сохраняется. 
   Преобразование к другому типу производится только при вызове метода \b utl::variant::to().
*/
class variant {
public:
   /*!   \brief   Создание экземпляра класса utl::variant с преобразованием данных из исходного типа 
                  в байтовый массив
         \tparam in_type - тип данных, из которого осуществялется преобразование
         \tparam transformer_class - класс, содержащий раелизации алгоритмов преобразования типов данных
   */
   template < class in_type, class transformer_class = defaultTransformer >
   static variant from ( const in_type & value_p ) {
      std::vector< storing_type > data_l;
      transformer_class().serialize( value_p, data_l );
      return variant ( data_l, type2id< in_type >::VALUE );
   }

   /*!   \brief Конструктор по умолчанию
      Не используется, так как для корректного использования неоходимо учесть тип размещаемых в классе 
      \b utl::variant данных.
   */
   variant () = delete;
   
   /*!   \brief Конструктор копирования
         \param etalon_cp - исходный экземпляр данных
   */
   variant ( const variant & etalon_cp ) : data_m ( etalon_cp.data_m ), typeId_m ( etalon_cp.typeId_m ) { ; }

   /*!   \brief Преобразование к данных по out_type по алгоритму, еализованному в классе transformer_class
         \tparam out_type - тип данных, к которому осуществялется преобразование
         \tparam transformer_class - класс, содержащий раелизации алгоритмов преобразования типов данных
   */
   template < class out_type, class transformer_class = defaultTransformer >
   out_type to () const {
      return transformer_class().cast< out_type >( data_m, typeId_m );
   }
   
   /*!   \brief Исходный тип хранимых данных
         \return Идентификатор типа из перечня, определённого в перечислении \b utl::typesIds_enum 
   */
   id16_t type () const { return typeId_m; }
private:
   storing_array data_m; ///< Массив для размещения данных
   id16_t typeId_m; ///< Идентификатор исходного типа, размещённых в массиве variant::data_m данных

   /*!   \brief Конструктор для создания экземпляра класса variant в статическом методе variant::from()
         \param data_cp - исходные данные, преобрахованные в байтовый массив
         \param typeId_p - тип хранимых данных
   */
   variant ( const storing_array & data_cp, int typeId_p ) : data_m ( data_cp ), typeId_m ( typeId_p ) { ; }
}; // class variant {
// ---------------------------------------------------------------------------------------------------------
} // namespace utl {
// ---------------------------------------------------------------------------------------------------------
#endif // UTL_VARIANT_H_MRV
// ---------------------------------------------------------------------------------------------------------